/**
 * @file AngrySpartyApp.h
 * @author Common Grackle
 *
 *
 */

#ifndef PROJECT1_ANGRYSPARTYAPP_H
#define PROJECT1_ANGRYSPARTYAPP_H

/**
* application class
*/
class AngrySpartyApp  : public wxApp {
private:

public:
    bool OnInit() override;

};

#endif //PROJECT1_ANGRYSPARTYAPP_H
