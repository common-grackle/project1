/**
 * @file Foe.h
 * @author hayeskhl, parkeu24
 *
 *
 */

#ifndef ANGRYSPARTY_FOE_H
#define ANGRYSPARTY_FOE_H

#include "Item.h"

/**
* Class for foe in the game
*/
class Foe : public Item {
private:
    /// The foe image file
    std::wstring mFoeImageFile;

    /// The underlying block image
    std::shared_ptr<wxImage> mFoeImage;

    /// The bitmap we can display for this block
    std::shared_ptr<wxBitmap> mFoeBitmap;

    /// The physics object installed into foe
    std::shared_ptr<Physics> mPhysics;

    /// True if the for has died, false otherwise
    bool mDead = false;

    /// Attributes loaded in from xml for foe
    double mX; ///> x position of foe
    double mY;  ///> y position of foe
    double mWidth;  ///> width of foe
    double mHeight; ///> height of foe
    double mAngle;  ///> angle of foe
    double mRadius; ///> radius of foe
    b2Vec2 mSize;   ///> size of foe
    b2Vec2 mPosition;   ///> position of foe
    double mFriction;   ///> friction of foe
    double mRestitution;    ///> restitution of foe
    double mDensity;    ///> density of foe
    int mDown = 0;  ///> whether foe is down 

    /// true if foe is static or false if it is dynamic
    bool mStatic;  

    /// The vertices of the foe
    std::vector<b2Vec2> mVertices;

public:
    /// Default constructor (disabled)
    Foe() = delete;

    /// Copy constructor
    Foe(const Foe &) = delete;

    /// Assignment operator
    void operator=(const Foe &) = delete;

    Foe(AngrySparty* sparty, wxXmlNode *node);

    void XmlLoad(wxXmlNode* node) override;

    void SetImage(const std::wstring& file) override;

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void InstallPhysics(std::shared_ptr<Physics> physics) override;

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */

    void Accept(Visitor* visitor) override { visitor->VisitFoe(this); }

    /**
     * Getter for the status of the for
     * @return bool true if foe has died,
     * false otherwise
     */
    bool GetFoeStatus(){return mDead;}

    /**
     * Sets foe dead state to false
     */
    void AliveAgain(){mDead = FALSE;}
};

#endif //ANGRYSPARTY_FOE_H
