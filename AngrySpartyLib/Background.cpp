/**
 * @file Background.cpp
 * @author ahmedna8
 */

#include "pch.h"
#include "Background.h"
#include "Consts.h"
#include <string>
#include <wx/graphics.h>
using namespace std;

/// Sand filename
const wstring SandImageName = L"images/sand.png";

/**
 * Constructor
 * @param angrySparty AngrySparty game this background is a member of
 * @param node the xml node that background is from
 */
Background::Background(AngrySparty* angrySparty, wxXmlNode *node) : Item(angrySparty)
{
    XmlLoad(node);

    mBackgroundImage = make_shared<wxImage>(mBackgroundImageName, wxBITMAP_TYPE_ANY);
    mBackgroundBitmap = make_shared<wxBitmap>(*mBackgroundImage);

}

/**
 * Draw this background
 * @param graphics graphics context to draw on
 */
void Background::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto wid = mWidth * Consts::MtoCM;
    auto hit = mHeight * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mBackgroundBitmap;

    graphics->PushState();
    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap,
                         -wid/2,
                         -hit,
                         wid, hit);

    graphics->PopState();
}

/** Loads the background attributes from xml
* @param node the xml node to load from
*/
void Background::XmlLoad(wxXmlNode* node)
{
    node->GetAttribute(L"width", L"0").ToDouble(&mWidth);
    node->GetAttribute(L"height", L"0").ToDouble(&mHeight);

    mBackgroundImageName = L"../images/" + node->GetAttribute(L"image");
}
