/**
 * @file SlingshotVisitor.h
 * @author NEW
 *
 * Slingshot visitor class
 */

#ifndef ANGRYSPARTY_SLINGSHOTVISITOR_H
#define ANGRYSPARTY_SLINGSHOTVISITOR_H

#include "Visitor.h"

/**
 * Slingshot visitor class
 */
class SlingshotVisitor : public Visitor{
private:
    /// slingshot object
    Slingshot* mSlingshot;
public:

};

#endif //ANGRYSPARTY_SLINGSHOTVISITOR_H
