/**
 * @file Level.h
 * @author parkeu24
 *
 * Base class for any level in the AngrySparty game
 */

#ifndef ANGRYSPARTY_LEVEL_H
#define ANGRYSPARTY_LEVEL_H

#include <wx/xml/xml.h> /// for wxXmlNode  might have to add to pch.h later
#include "AngrySparty.h"

class Item;
class SpartyBird;

/**
 * Base class for any level in the game
 */
class Level {
private:

    /// vector of items for this level
    std::vector<std::shared_ptr<Item>> mItems;

    /// vector of birds for this level
    std::vector<std::shared_ptr<SpartyBird>> mBirds;

    /// height of this level
    double mWidth;

    /// width of this level
    double mHeight;


public:
    /**
    * Sets the levels width
    * @param width the width to set the level to
    */
    void SetLevelWidth(double width){mWidth = width;}

    /**
    * Sets the levels height
    * @param height the height to set the level to
    */
    void SetLevelHeight(double height){mHeight = height;}

    /**
    * Gets the levels width 
    * @return mWidth the width of the level
    */
    double GetLevelWidth(){return mWidth;}

    /**
    * Gets the levels height 
    * @return mHeight the height of the level
    */
    double GetLevelHeight(){return mHeight;}
    
    void AddItem(std::shared_ptr<Item> item);
    void AddBird(std::shared_ptr<SpartyBird> bird);

    void XmlItem(AngrySparty* angrySparty, wxXmlNode *node);
    void XmlBird(AngrySparty* angrySparty, wxXmlNode *node);

    void Install(AngrySparty* game);

    void Destroy();

    void Accept(Visitor* visitor);

};

#endif //ANGRYSPARTY_LEVEL_H
