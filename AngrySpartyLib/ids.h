/**
 * @file ids.h
 * @author ahmedna8
 *
 * ID values for our program
 */

#ifndef PROJECT1_IDS_H
#define PROJECT1_IDS_H

/**
 * ID values for our program
 */
enum IDs {
    IDM_LEVELZERO = wxID_HIGHEST + 1,
    IDM_LEVELONE,
    IDM_LEVELTWO,
    IDM_LEVELTHREE,
    IDM_DEBUGDRAW
};

#endif //PROJECT1_IDS_H
