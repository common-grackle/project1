/**
 * @file Item.h
 * @author hayeskhl, parkeu24
 *
 *
 */

#ifndef PROJECT1_ITEM_H
#define PROJECT1_ITEM_H

#include <string>
#include <memory>
#include <wx/xml/xml.h>
#include <wx/graphics.h>
#include "SpartyBird.h"
#include "Visitor.h"


class Physics;
class b2Body;
class b2World;
class b2Fixture;
class b2Shape;

class AngrySparty;

/**
 * Base class for any item in our game
 */
class Item {
private:
    /// The game this item is contained to
    AngrySparty *mAngrySparty;

    /// The world this item belongs to
    b2World* mWorld;

    /// Item location in the game
    double mX;  ///< X location for the center of the item
    double mY;  ///< Y location for the center of the item

    /// The image for this item
    std::unique_ptr<wxImage> mItemImage;

    /// The bitmap for this item
    std::unique_ptr<wxBitmap> mItemBitmap;

    /// The file for this item
    std::wstring mFile;

    /// The physics body of this item
    b2Body *mBody = nullptr;


protected:
    Item(AngrySparty *sparty);

public:
    virtual ~Item();

    /// Default constructor (disabled)
    Item() = delete;

    /// Copt constructor (disabled)
    Item(const Item &) = delete;

    /// Assignment operator
    void operator=(const Item &) = delete;

    /**
     * The X location of the item
     * @return X location in pixels
     */
    double GetX() const { return mX; }

    /**
     * The Y location of the item
     * @return Y location in pixels
     */
    double GetY() const { return mY; }

    /**
     * Getter for the body
     * @return mBody a b2Body
     */
    b2Body* GetBody(){return mBody;}

    /**
     * A setter for the body
     * @param body the body to set the body to
     */
    void SetBody(b2Body* body){mBody = body;}

    /**
     * Set the item location
     * @param x X location in pixels
     * @param y Y location in pixels
     */
    void SetLocation(double x, double y) { mX = x; mY = y; }

    virtual void SetImage(const std::wstring &file);

    virtual void XmlLoad(wxXmlNode *node);

    /**
    * Draw this item
    * @param graphics graphic context to draw on
    */
    virtual void Draw(std::shared_ptr<wxGraphicsContext> graphics) = 0;

    /**
     * Virtual install Physics function that items will override
     * @param physics the physics to install
     */
    virtual void InstallPhysics(std::shared_ptr<Physics> physics){}

    /**
     * Virtual function to update the items
     * items will override
     * @param elapsed time to update
     */
    virtual void Update(double elapsed){}

    /**
     * Sets the bird that goes w the item
     * only used in slingshot
     * @param spartyBird bird that will go w slingshot
     */
    virtual void SetBird(std::shared_ptr<SpartyBird> spartyBird) {};

    /**
     * Draws the item again when loading a bird
     * only used by slingshot item
     * @param graphics graphics context used to draw
     */
    virtual void Draw2(std::shared_ptr<wxGraphicsContext> graphics) {};

    /**
     * redraws the items background state
     * only used in slingshot
     * @param graphics graphics context used to draw
     */
    virtual void BackgroundState(std::shared_ptr<wxGraphicsContext> graphics) {};

    /**
     * redraws the items left band state
     * only used in slingshot
     * @param graphics graphics context used to draw
     */
    virtual void LeftBandState(std::shared_ptr<wxGraphicsContext> graphics) {};

    /**
    * redraws the items bird state
    * only used in slingshot
    */
    virtual void BirdState() {};

    /**
     * redraws the items right band state
     * only used in slingshot
     * @param graphics graphics context used to draw
     */
    virtual void RightBandState(std::shared_ptr<wxGraphicsContext> graphics) {};

    /**
     * redraws the items final state
     * only used in slingshot
     * @param graphics graphics context used to draw
     */
    virtual void FinalState(std::shared_ptr<wxGraphicsContext> graphics) {};

    /**
    * Test this item to see if it has been clicked on
    * @param x X location on the game to test in pixels
    * @param y Y location on the game to test in pixels
    * @return true if clicked on
    */
    virtual bool HitTest(double x, double y) {return false;}

    /**
     * Sets the bird Location
     * @param x location x of bird
     * @param y location y of bird
     */
    virtual void SetBirdLocation(double x, double y) {};

    /**
     * Sets the item as a dynamic item
     * @param physics used to set as dynamic physics body
     */
    virtual void SetDynamic(std::shared_ptr<Physics> physics){};

    /**
     * Sets the game an item belongs to
     * @param Game to set the item to be in
     */
    virtual void SetAngrySparty(AngrySparty* Game){mAngrySparty = Game;}

    /**
     * Getter for AngrySparty/ the game
     * @return AngrySparty the game item belongs to
     */
    AngrySparty* GetAngrySparty(){return mAngrySparty;}

    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    virtual void Accept(Visitor* visitor){}

    /**
     * Set Loaded to false on level reset
     */
    virtual void SetLoadedFalse(){};

    /**
     * Reset Foe Count
     */
    virtual void AliveAgain(){};

    /**
     * Reset Tnt block explosive state
     */
    virtual void PrimeTnt(){};


};

#endif //PROJECT1_ITEM_H

