/**
 * @file AngrySpartyView.h
 * @author ahmedna8, chenan12, parkeu24
 *
 *
 */

#ifndef PROJECT1_ANGRYSPARTYVIEW_H
#define PROJECT1_ANGRYSPARTYVIEW_H

#include "AngrySparty.h"

/**
* Base class for view of the game
*/
class AngrySpartyView : public wxWindow {
private:
    /// the game we are in 
    AngrySparty mAngrySparty;
   
    /// Physics item pointer for game
    std::shared_ptr<Physics> mPhysics;

    /// Any item we are currently dragging
    std::shared_ptr<Item> mGrabbedItem;

    /// Stopwatch used to measure elapsed time
    wxStopWatch mStopWatch;

    /// The last stopwatch time
    double mTime = 0;

    /// keeps track of timer
    wxTimer mTimer;


public:
    void Initialize(wxFrame *parent);

    void OnPaint(wxPaintEvent& event);

    void Load(wxString filename);

    void LoadLevel(int level);

    void OnTimer(wxTimerEvent& event);

    void InitializeLoad();

    /** Loads level 0
    * @param event the event that triggers the loading
    * of level 0
    */
    void LoadLevelZero(wxCommandEvent& event){ LoadLevel(0);};

    /** Loads level 1
    * @param event the event that triggers the loading
    * of level 1
    */
    void LoadLevelOne(wxCommandEvent& event){ LoadLevel(1);};

    /** Loads level 2
    * @param event the event that triggers the loading
    * of level 2
    */
    void LoadLevelTwo(wxCommandEvent& event){ LoadLevel(2);};

    /** Loads level 3
    * @param event the event that triggers the loading
    * of level 3
    */
    void LoadLevelThree(wxCommandEvent& event){ LoadLevel(3);};

    /** 
    * Stops the timer
    */
    void Stop() {mTimer.Stop();}

    void OnLeftDown(wxMouseEvent &event);
    void OnLeftUp(wxMouseEvent &event);
    void OnMouseMove(wxMouseEvent &event);

};


#endif //PROJECT1_ANGRYSPARTYVIEW_H
