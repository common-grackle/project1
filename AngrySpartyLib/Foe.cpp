/**
 * @file Foe.cpp
 * @author hayeskhl, parkeu24
 */

#include "pch.h"
#include "Foe.h"
#include "Physics.h"
#include "AngrySparty.h"
#include <wx/graphics.h>
#include "Consts.h"

using namespace std;

/**
 * Constructor
 * @param sparty game this foe is a member of
 * @param node the node that this foe is from
 */
Foe::Foe(AngrySparty* sparty, wxXmlNode *node) : Item(sparty)
{
    XmlLoad(node);
    SetImage(mFoeImageFile);
}

/**
 * brief Load the attributes for an item node.
 * @param node The Xml node we are loading the item from
 */
void Foe::XmlLoad(wxXmlNode* node)
{
    // Loaded x and y position of the item
    Item::XmlLoad(node);

    node->GetAttribute(L"angle", L"0").ToDouble(&mAngle);
    node->GetAttribute(L"radius", L"0").ToDouble(&mRadius);
    node->GetAttribute(L"down", L"0").ToInt(&mDown);
    mFoeImageFile = node->GetAttribute(L"image");

    for (int i =0; i < 8; i++){
        double r = i;
        double angle = r/8*b2_pi*2;

        mVertices.push_back(b2Vec2(mRadius * cos( angle), mRadius * sin( angle)));
    }   
}

/** Sets the foe image
* @param file the file of the image to use for foe
*/
void Foe::SetImage(const std::wstring& file)
{
    wstring filename = L"images/" + file;
    mFoeImage = make_shared<wxImage>(filename, wxBITMAP_TYPE_ANY);
    mFoeBitmap= make_shared<wxBitmap>(*mFoeImage);

    mFoeImageFile = file;
}

/** Installs physics into the foe object
* @param physics the physics object installed in foe
*/
void Foe::InstallPhysics(std::shared_ptr<Physics> physics)
{
    mPhysics = physics;
    b2World* world = mPhysics->GetWorld();

    mPosition = b2Vec2(GetX() , GetY());
    mSize = b2Vec2(mWidth, mHeight);

    // Create the poly
    b2PolygonShape poly;
    poly.Set(&mVertices[0], mVertices.size());

    // Create the body definition
    b2BodyDef bodyDefinition;
    bodyDefinition.position = mPosition;
    bodyDefinition.angle = mAngle * Consts::DtoR;
    bodyDefinition.type = b2_dynamicBody;
    auto body = world->CreateBody(&bodyDefinition);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &poly;
    fixtureDef.density = (float)5;
    fixtureDef.friction = 1;
    fixtureDef.restitution = 0.3;

    body->CreateFixture(&fixtureDef);

    SetBody(body);
}

/** Draws a foe object
* @param graphics the graphics object used to draw a foe
*/
void Foe::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    graphics->PushState();

    auto body = GetBody();
    auto position = body->GetPosition();
    auto angle = body->GetAngle();

    auto wid = mRadius * Consts::MtoCM * 2;
    auto x = position.x * Consts::MtoCM;
    auto y = position.y * Consts::MtoCM;

    graphics->Translate(x, y);
    graphics->Rotate(angle);

    std::shared_ptr<wxBitmap> bitmap = mFoeBitmap;

    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap,
            -wid/2,
            -wid/2,
            wid, wid);

    if(body->GetType() == b2_dynamicBody && body->GetPosition().y + mRadius < mDown && body->GetPosition().y > -999)
    {
        body->SetTransform(b2Vec2(-1000,-1000), mAngle);
        GetAngrySparty()->GetScoreBoard()->IncreaseScore();
        mDead= true;
    }

    graphics->PopState();
}


