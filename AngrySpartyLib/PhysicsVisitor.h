/**
 * @file PhysicsVisitor.h
 * @author chenan12
 *
 *
 */

#ifndef ANGRYSPARTY_PHYSICSVISITOR_H
#define ANGRYSPARTY_PHYSICSVISITOR_H

class b2World;
#include "Physics.h"
#include "Visitor.h"


/**
* physics visitor class to visit physics bodies
*/
class PhysicsVisitor : public Visitor{
private:
    /// pointer to a body for this physics visitor
    b2Body* mBody;
public:

    /**
     * Visits the physics object
     * @param physics object to visit
     */
    virtual void VisitPhysics(Physics *physics) override;

    /**
     * Sets physics body
     * @param body physics body to set
     */
    void SetBody(b2Body* body){mBody = body;}

    /**
    * Gets the body this physics visitor is referring to
    * @return mBody the body referred to
    */
    b2Body* GetBody(){return mBody;}
};

#endif //ANGRYSPARTY_PHYSICSVISITOR_H
