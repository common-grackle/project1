/**
 * @file TntBlock.cpp
 * @author NEW
 *
 * TNT Block Class
 */

#include "pch.h"
#include "TntBlock.h"
#include "Physics.h"
#include "AngrySparty.h"
#include <wx/graphics.h>
#include "Consts.h"
#include "DebugDraw.h"

using namespace std;

/**
 * Constructor for TNT Block
 * @param sparty
 * @param node
 */
TntBlock::TntBlock(AngrySparty* sparty, wxXmlNode *node) : Item(sparty)
{
    XmlLoad(node);
    SetImage(mTntImageFile);

}

/**
* Sets the image for the the tnt block
* @param file the image file to use for the block
*/
void TntBlock::SetImage(const std::wstring& file)
{
    wstring filename = L"images/" + file;
    mTntImage = make_shared<wxImage>(filename, wxBITMAP_TYPE_ANY);
    mTntBitmap = make_shared<wxBitmap>(*mTntImage);

    mTntImageFile = file;
}

/**
* brief Load the attributes for an item node.
* @param node The Xml node we are loading the item from
*/
void TntBlock::XmlLoad(wxXmlNode* node)
{
    // Loaded x and y position of the item
    Item::XmlLoad(node);

    node->GetAttribute(L"width", L"0").ToDouble(&mWidth);
    node->GetAttribute(L"height", L"0").ToDouble(&mHeight);
    node->GetAttribute(L"angle", L"0").ToDouble(&mAngle);
    node->GetAttribute(L"friction", L"0.5").ToDouble(&mFriction);
    node->GetAttribute(L"restitution", L"0.5").ToDouble(&mRestitution);
    node->GetAttribute(L"density", L"1").ToDouble(&mDensity);

    mType = node->GetAttribute(L"type");
    mTntImageFile = node->GetAttribute(L"image");

    SetImage(mTntImageFile);
}

/**
* Installs the physics into the block
* @param physics the physics object being installed
* into the block
*/
void TntBlock::InstallPhysics(std::shared_ptr<Physics> physics)
{
    mPhysics = physics;
    b2World* world = mPhysics->GetWorld();


    mSize = b2Vec2(mWidth , mHeight);
    mPosition = b2Vec2(GetX() , GetY());

    // Create the box
    b2PolygonShape box;
    box.SetAsBox(mSize.x/2, mSize.y/2);

    // Create the body definition
    b2BodyDef bodyDefinition;
    bodyDefinition.position = mPosition;
    bodyDefinition.angle = mAngle * Consts::DtoR;
    bodyDefinition.type = b2_dynamicBody;
    auto body = world->CreateBody(&bodyDefinition);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = (float)mDensity;
    fixtureDef.friction = (float)mFriction;
    fixtureDef.restitution = (float)mRestitution;

    body->CreateFixture(&fixtureDef);

    SetBody(body);

}

/**
* Draws the tnt block object
* @param graphics graphics object to draw the block item
*/
void TntBlock::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    graphics->PushState();

    auto body = GetBody();


    auto position = body->GetPosition();
    auto angle = body->GetAngle();

    graphics->Translate(position.x * Consts::MtoCM,
            position.y * Consts::MtoCM);
    graphics->Rotate(angle);

    double x = -(mSize.x / 2) * Consts::MtoCM;

    double y = (mSize.y / 2) * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mTntBitmap;


    graphics->Translate(0, y);
    graphics->Scale(1, -1);


    graphics->DrawBitmap(*bitmap,x,0,mSize.x * Consts::MtoCM, mSize.y * Consts::MtoCM);

    if(abs(body->GetLinearVelocity().x) > 1 || abs(body->GetLinearVelocity().y) > 1)
    {
        body->SetTransform(b2Vec2(-1000,-1000), mAngle);
        if(!mExploded)
        {
            for(int i = 0; i < 360; ++i)
            {
                if(i % 20 == 0)
                {
                    InstallTnt(mPhysics, i);
                }
                if(i % 15 == 0)
                {
                    InstallTnt(mPhysics, -i);
                }
            }
            mExploded = TRUE;
        }

    }

    graphics->PopState();
}

/**
* Installs the Tnt explosion physics
* @param physics the physics object being installed
* into the block
 * @param direction the direction the tnt needs
*/

void TntBlock::InstallTnt(std::shared_ptr<Physics> physics, int direction)
{
    mPhysics = physics;
    b2World* world = mPhysics->GetWorld();

    mPosition = b2Vec2(GetX() , GetY());

    // Create the box
    b2PolygonShape box;
    box.SetAsBox(0.5, 0.5);

    // Create the body definition
    b2BodyDef bodyDefinition;
    bodyDefinition.position = mPosition;
    bodyDefinition.angle = mAngle * Consts::DtoR;
    bodyDefinition.type = b2_kinematicBody;
    auto body = world->CreateBody(&bodyDefinition);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = (float)2;
    fixtureDef.friction = (float)0.3;
    fixtureDef.restitution = (float)0.3;

    body->CreateFixture(&fixtureDef);

    body->SetLinearVelocity(b2Vec2(direction,direction));

//    SetBody(body);
}

