/**
 * @file TntBlock.h
 * @author NEW
 *
 * TNT block class
 */

#ifndef ANGRYSPARTY_TNTBLOCK_H
#define ANGRYSPARTY_TNTBLOCK_H

#include "Item.h"
#include <memory>

/**
 * TNT Block Class
 */
class TntBlock : public Item {
private:
    /// The block image file
    std::wstring mTntImageFile;

    /// The underlying block image
    std::shared_ptr<wxImage> mTntImage;

    /// The bitmap we can display for this block
    std::shared_ptr<wxBitmap> mTntBitmap;

    /// The physics object installed into block
    std::shared_ptr<Physics> mPhysics;


    /// Block attribute to be loaded from the XML file
    double mWidth; ///< width of the block
    double mHeight; ///< height of the block
    b2Vec2 mSize;   ///< size of the block
    b2Vec2 mPosition;   ///< position of the block
    double mAngle;  ///< angle of the block
    double mFriction;   ///< friction of the block
    double mRestitution; ///< restitution of the block
    double mDensity;    ///< density of the block
    std::wstring mType; ///< Type of the block

    bool mExploded = FALSE; ///< Whether the block has been exploded or not

public:

    /// Default constructor (disabled)
    TntBlock() = delete;

    /// Copy constructor
    TntBlock(const TntBlock &) = delete;

    /// Assignment operator
    void operator=(const TntBlock &) = delete;

    TntBlock(AngrySparty* sparty, wxXmlNode *node);

    void XmlLoad(wxXmlNode* node) override;

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void InstallPhysics(std::shared_ptr<Physics> physics) override;

    void InstallTnt(std::shared_ptr<Physics> physics, int angle);

    void SetImage(const std::wstring& file) override;

    /**
     * Resets the tnt state to nit exploded
     */
    void PrimeTnt(){mExploded = FALSE;}

};

#endif //ANGRYSPARTY_TNTBLOCK_H
