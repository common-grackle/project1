/**
 * @file Background.h
 * @author ahmedna8
 *
 *
 */

#ifndef ANGRYSPARTY_BACKGROUND_H
#define ANGRYSPARTY_BACKGROUND_H

#include "Item.h"

/**
* Background class of game
*/
class Background  : public Item {
private:
    /// The underlying background image
    std::shared_ptr<wxImage> mBackgroundImage;

    /// The bitmap we can display for the background
    std::shared_ptr<wxBitmap> mBackgroundBitmap;

    /// background width loaded from xml node
    double mWidth;

    /// background height loaded from xml node
    double mHeight;

    /// background image name
    wxString mBackgroundImageName;

public:
    /// Default constructor (disabled)
    Background() = delete;

    /// Copy constructor (disabled)
    Background(const Background &) = delete;

    /// Assignment operator
    void operator=(const Background &) = delete;

    Background(AngrySparty* angrySparty, wxXmlNode *node);

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void XmlLoad(wxXmlNode* node) override;


};


#endif //ANGRYSPARTY_BACKGROUND_H
