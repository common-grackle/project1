/**
 * @file HelmetSpartyBird.cpp
 * @author hayeskhl
 */

#include "pch.h"
#include "HelmetSpartyBird.h"
#include "Consts.h"
#include <wx/graphics.h>

using namespace std;

/// The image to use for this sparty
const std::wstring HelmetSpartyImage = L"helmet-sparty.png";

/// The radius in meters
const double HelmetSpartyRadius = 0.25;

/// The velocity factor for Helmet Sparty
const float HelmetSpartyVelocityFactor = 20.0;

/**
 * Constructor
 * @param sparty game this bird is a member of
 * @param node node this bird came from
 */
HelmetSpartyBird::HelmetSpartyBird(AngrySparty* sparty, wxXmlNode *node) : SpartyBird(sparty, node)
{
    XmlLoad(node);
    SetImage(HelmetSpartyImage);
}

/**
 * Sets the image file for the bird
 * @param file the image file of the bird
 */
void HelmetSpartyBird::SetImage(const std::wstring& file)
{
    SpartyBird::SetImage(HelmetSpartyImage);
}

/**
 * brief Load the attributes for this bird node.
 * @param node The Xml node we are loading the item from
 */
void HelmetSpartyBird::XmlLoad(wxXmlNode* node)
{
    SpartyBird::XmlLoad(node);
}

/** Gets the birds velocity
* @return double the velocity of the gruff sparty bird
*/
double HelmetSpartyBird::GetVelocity()
{
    mVelocity = HelmetSpartyVelocityFactor;
    return mVelocity;
}




