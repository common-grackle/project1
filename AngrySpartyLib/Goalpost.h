/**
 * @file Goalpost.h
 * @author ahmedna8
 *
 */

#ifndef ANGRYSPARTY_GOALPOST_H
#define ANGRYSPARTY_GOALPOST_H

#include "Item.h"

#include <string>

/**
* Class for goalpost in the game
*/
class Goalpost  : public Item {
private:
    /// pointer to the bird that slingshot knows
    std::shared_ptr<SpartyBird> mCurrentBird;

    /// The underlying slingshot image
    std::shared_ptr<wxImage> mGoalpostImage;

    /// image of front slingshot "prong"
    std::shared_ptr<wxImage> mGoalpostFrontImage;

    /// The bitmap we can display for this slingshot
    std::shared_ptr<wxBitmap> mGoalpostBitmap;

    /// The bitmap we can display for the front "prong"
    std::shared_ptr<wxBitmap> mGoalpostFrontBitmap;

    /// x position of goalpost
    double mX = 0;

    /// y position of goalpost
    double mY = 0;

    /// Tells if birds loaded into goalpost
    bool mLoaded = FALSE;

    /// Tells if slingshot is drawn
    bool mDrawSling = TRUE;


public:
    /// Default constructor (disabled)
    Goalpost() = delete;

    /// Copy constructor
    Goalpost(const Goalpost &) = delete;

    /// Assignment operator
    void operator=(const Goalpost &) = delete;

    Goalpost(AngrySparty* angrySparty, wxXmlNode *node);

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void SetBird(std::shared_ptr<SpartyBird> spartyBird) override;
    void BackgroundState(std::shared_ptr<wxGraphicsContext> graphics) override;
    void LeftBandState(std::shared_ptr<wxGraphicsContext> graphics) override;
    void BirdState() override;
    void RightBandState(std::shared_ptr<wxGraphicsContext> graphics) override;
    void FinalState(std::shared_ptr<wxGraphicsContext> graphics) override;
    bool HitTest(double x, double y) override;

    void SetBirdLocation(double x, double y) override;
    void SetDynamic(std::shared_ptr<Physics> physics) override;

    void Draw2(std::shared_ptr<wxGraphicsContext> graphics) override;

    /**
     * Set Loaded to false on level reset
     */
    void SetLoadedFalse(){mLoaded = FALSE;}

    void XmlLoad(wxXmlNode* node) override;

};


#endif //ANGRYSPARTY_GOALPOST_H
