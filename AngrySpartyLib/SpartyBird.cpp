/**
 * @file SpartyBird.cpp
 * @author hayeskhl
 */

#include "pch.h"
#include "SpartyBird.h"
#include <wx/graphics.h>
#include "Consts.h"
#include "Physics.h"
#include "Slingshot.h"
#include "SlingshotVisitor.h"

using namespace std;

/**
 * Constructor
 * @param sparty game this sparty bird is a member of
 * @param node the node this bird is from
 */
SpartyBird::SpartyBird(AngrySparty* sparty, wxXmlNode *node) : mAngrySparty(sparty)
{
    XmlLoad(node);
}

/**
 * Destructor
 */
SpartyBird::~SpartyBird()
{
}

/**
 * Sets the image file for the bird
 * @param file file to set image with
 */
void SpartyBird::SetImage(const std::wstring &file)
{
    wstring filename = L"images/" + file;
    mSpartyBirdImage = make_shared<wxImage>(filename, wxBITMAP_TYPE_ANY);
    mSpartyBirdBitmap = make_shared<wxBitmap>(*mSpartyBirdImage);

}

/**
 * Draw the bird
 * @param graphics the graphics object used to draw
 */
void SpartyBird::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto body = GetBody();

    auto position = body->GetPosition();
    auto angle = body->GetAngle();


    auto wid = mRadius * Consts::MtoCM * 2;
    auto x = position.x * Consts::MtoCM;
    auto y = position.y * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mSpartyBirdBitmap;

    graphics->PushState();
    graphics->Translate(x, y);
    graphics->Rotate(angle);

    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap,
            -wid/2,
            -wid/2,
            wid, wid);


    if(body->GetType() == b2_dynamicBody && abs(body->GetLinearVelocity().x) <= 0.1 && abs(body->GetLinearVelocity().y) <= 0.1)
    {
        body->SetTransform(b2Vec2(-1000,-1000), mAngle);
        mAngrySparty->IncrementCount();
        //Function to move bird to back of mBirds
        mDead = true;
    }


    graphics->PopState();
}

/**
 * brief Load the attributes for an item node.
 * @param node The Xml node we are loading the item from
 */
void SpartyBird::XmlLoad(wxXmlNode* node)
{
    node->GetAttribute(L"x", L"0").ToDouble(&mX);
    node->GetAttribute(L"y", L"0").ToDouble(&mY);
    node->GetAttribute(L"spacing", L"0").ToDouble(&mSpacing);
    mOriginalX = mX;
    mOriginalY = mY;
    mOriginalSpacing = mSpacing;
}

/**
 * Finds distance between two birds
 * @param bird to compare distance to
 * @return distance
 */
double SpartyBird::DistanceTo(std::shared_ptr<SpartyBird> bird)
{
    auto dx = bird->GetX() - GetX();
    auto dy = bird->GetY() - GetY();
    return sqrt(dx * dx + dy * dy);
}

/**
 * Sets the location of the bird so they don't overlap
 * @param x the x location of the bird
 * @param y the y location of the bird
 */
void SpartyBird::SetLocation(double x, double y)
{
    mX = x;
    mY = y;
}


/**
 * Installs physics for the birds
 * @param physics the physics body to add
 */
void SpartyBird::InstallPhysics(std::shared_ptr<Physics> physics)
{
    mPhysics = physics;
    b2World* world = physics->GetWorld();

    // Create the body definition
    b2BodyDef bodyDefinition;
    bodyDefinition.position = b2Vec2(mX,mY);
    bodyDefinition.type = b2_staticBody;
    auto body = world->CreateBody(&bodyDefinition);

    // Create the shape
    b2CircleShape circle;

    circle.m_radius = (float)mRadius;

    body->CreateFixture(&circle, 0.0f);



    // Get the position and angle before
    // we destroy the body.

    // Destroy the body in the physics system
    if(mMoveable)
    {
        auto position = body->GetPosition();
        auto angle = body->GetAngle();

        mAngle = angle;

        b2Body* currBody = GetBody();

        // doesn't throw an error when commented out
        // when trying to reload the same level
        world->DestroyBody(currBody);
        world->DestroyBody(body);

        // Create the body definition
        b2BodyDef bodyDefinition2;
        bodyDefinition2.position = position;
        bodyDefinition2.angle = mAngle * Consts::DtoR;
        bodyDefinition2.type = b2_dynamicBody;
        bodyDefinition2.angularDamping = 0.9;
        bodyDefinition2.linearDamping = 0.1;
        body = world->CreateBody(&bodyDefinition2);

        // Create the shape
        b2CircleShape circle2;
        circle2.m_radius = (float) mRadius;

        b2FixtureDef fixtureDef;
        fixtureDef.shape = &circle;
        fixtureDef.density = (float) 5;
        fixtureDef.friction = 1;
        fixtureDef.restitution = 0.3;

        body->CreateFixture(&fixtureDef);

        auto direction = b2Vec2(mLoadX,mLoadY) - b2Vec2(mX,mY);

        direction *= GetVelocity();
        body->SetLinearVelocity(direction);

    }
    SetBody(body);
}

/**
 * Test to see if we hit this object with a mouse.
 * @param x X position to test
 * @param y Y position to test
 * @return true if hit.
 */
bool SpartyBird::HitTest(double x, double y)
{

//    double nx = x / Consts::MtoCM;
//    double ny = y / Consts::MtoCM;

    auto fixture = GetBody()->GetFixtureList();
    for( ; fixture != nullptr; fixture = fixture->GetNext())
    {
        if(fixture->TestPoint(b2Vec2(x, y)))
        {
            return true;
        }
    }
    return false;

}

/**
 * Sets the birds physic body location
 * @param x the x location of bird body
 * @param y the y location of bird body
 */
void SpartyBird::SetBodyLocation(double x, double y)
{
    mX = x;
    mY = y;

    b2Body* body = GetBody();
    body->SetTransform(b2Vec2(mX, mY),body->GetAngle());
    SetBody(body);
}

/**
 * Sets the bird to static when reloading the level already played
 */
 void SpartyBird::SetDefault()
 {
     mMoveable = FALSE;
     mX = mOriginalX;
     mY = mOriginalY;
     mSpacing = mOriginalSpacing;
 }

