/**
 * @file FoeDieCount.h
 * @author hayeskhl
 *
 * Class that visits the Foes
 * and counts the total foes for a level
 * and the dead foes in a level
 */

#ifndef ANGRYSPARTY_FOEDIECOUNT_H
#define ANGRYSPARTY_FOEDIECOUNT_H

#include "Visitor.h"
#include "Foe.h"

/**
 * Foe Visitor Class
 */
class FoeDieCount : public Visitor{
private:
    /// Foe died counter
    int mDiedFoe = 0;

    /// Foe counter
    int mFoe = 0;

public:
    /**
     * Get the number of died Foe
     * @return Number of died Foe
     */
    int GetNumFoes() const { return mDiedFoe; }

    /**
     * Resets the dead foe count
     */
    void ResetDeadFoes(){mDiedFoe = 0;}

    /**
     * Resets the foe count
     */
    void ResetFoeCount(){mFoe = 0;}

    void  VisitFoe(Foe* foe);

    /**
     * gets the total number of foes in a level
     * @return int total number of foes
     */
    int TotalFoes(){return mFoe;}

};

#endif //ANGRYSPARTY_FOEDIECOUNT_H
