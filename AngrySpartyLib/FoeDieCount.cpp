/**
 * @file FoeDieCount.cpp
 * @author hayeskhl
 * Class that visits the Foes
 * and counts the total foes for a level
 * and the dead foes in a level
 */

#include "pch.h"
#include "FoeDieCount.h"
#include "Foe.h"

#include <iostream>

using namespace std;

/**
 * Visits a foe and increments death count
 * if foe is dead and increments total
 * number of foes in a level
 * @param foe the foe being visited
 */
void  FoeDieCount::VisitFoe(Foe* foe){
    mFoe++;
    if(foe->GetFoeStatus()==true)
    {
        mDiedFoe++;
    }
}
