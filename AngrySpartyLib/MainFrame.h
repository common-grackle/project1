/**
 * @file MainFrame.h
 * @author chenan12
 *
 *
 */

class AngrySpartyView;

#ifndef PROJECT1_MAINFRAME_H
#define PROJECT1_MAINFRAME_H

/**
 * The top-level (main) frame of the application
 */
class MainFrame : public wxFrame {
private:
    /// pointer to the games view
    AngrySpartyView *mAngrySpartyView;

public:

    // initialization
    void Initialize();

    void OnExit(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

    void OnAbout(wxCommandEvent& event);



};

#endif //PROJECT1_MAINFRAME_H
