/**
 * @file ScoreCounter.h
 * @author parkeu24
 *
 * Class for keeping track of the score
 */

#ifndef ANGRYSPARTY_SCORECOUNTER_H
#define ANGRYSPARTY_SCORECOUNTER_H

/**
 * Class for counting the score for the AngrySparty game
 */
class ScoreCounter {
private:
    int mScore; ///< user's score for the game
public:
    /// Default constructor (disabled)
    ScoreCounter() = delete;

    /// Copy constructor (disabled)
    ScoreCounter(const ScoreCounter &) = delete;

    /**
     * Increment the score
     */
    void operator++()
    {
        mScore++;
    }
};

#endif //ANGRYSPARTY_SCORECOUNTER_H
