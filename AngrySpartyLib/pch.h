/**
 * @file pch.h
 * @author Common Grackle
 *
 *
 */

#ifndef PROJECT1_PCH_H
#define PROJECT1_PCH_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <box2d/b2_world.h>
#include <box2d/box2d.h>

#endif //PROJECT1_PCH_H
