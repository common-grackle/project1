/**
 * @file MainFrame.cpp
 * @author chenan12, parkeu24
 */

#include "pch.h"
#include "MainFrame.h"
#include "AngrySpartyView.h"
#include "ids.h"

/**
 * Initialize the MainFrame window.
 */
void MainFrame::Initialize()
{
    Create(nullptr, wxID_ANY, L"AngrySparty",
            wxDefaultPosition,  wxSize( 1000,800 ));

    // Create a sizer that will lay out child windows vertically
    // one above each other
    auto sizer = new wxBoxSizer( wxVERTICAL );

    // Create the view class object as a child of MainFrame
    auto angrySpartyView = new AngrySpartyView();
    angrySpartyView->Initialize(this);

    mAngrySpartyView = angrySpartyView;

    // Add it to the sizer
    sizer->Add(angrySpartyView,1, wxEXPAND | wxALL );

    // Set the sizer for this frame
    SetSizer( sizer );

    // Layout (place) the child windows.
    Layout();

    auto menuBar = new wxMenuBar( );

    auto fileMenu = new wxMenu();
    auto levelMenu = new wxMenu();
    auto viewMenu = new wxMenu();
    auto helpMenu = new wxMenu();

    menuBar->Append(fileMenu, L"&File" );
    menuBar->Append(levelMenu, L"&Level");
    menuBar->Append(viewMenu, L"&View");
    menuBar->Append(helpMenu, L"&Help");

    levelMenu->Append(IDM_LEVELZERO, L"&Level 0", L"Go to Level 0");
    levelMenu->Append(IDM_LEVELONE, L"&Level 1", L"Go to Level 1");
    levelMenu->Append(IDM_LEVELTWO, L"&Level 2", L"Go to Level 2");
    levelMenu->Append(IDM_LEVELTHREE, L"&Level 3", L"Go to Level 3");

    viewMenu->Append(IDM_DEBUGDRAW, L"&Debug Draw", L"Debug View");

    fileMenu->Append(wxID_EXIT, "E&xit\tAlt-X", "Quit this program");
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnExit, this, wxID_EXIT);
    Bind(wxEVT_CLOSE_WINDOW, &MainFrame::OnClose, this);

    helpMenu->Append(wxID_ABOUT, "&About\tF1", "Show about dialog");
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnAbout, this, wxID_ABOUT);


    SetMenuBar( menuBar );

    CreateStatusBar( 1, wxSTB_SIZEGRIP, wxID_ANY );

}

/**
 * About menu option handlers
 * @param event the about event
 */
void MainFrame::OnAbout(wxCommandEvent& event)
{
    wxMessageBox(L"About Angry Sparty by Common Grackle",
            L"Angry Sparty",
            wxOK,
            this);
}

/**
* Handles the event on exit 
* @param event the exit event
*/
void MainFrame::OnExit(wxCommandEvent& event)
{
    mAngrySpartyView->Stop();
    Destroy();
}

/**
* Handles the event on close 
* @param event the close event
*/
void MainFrame::OnClose(wxCloseEvent& event)
{
    mAngrySpartyView->Stop();
    Destroy();
}
