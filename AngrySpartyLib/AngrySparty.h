/**
 * @file AngrySparty.h
 * @author ahmedna8, chenan12, parkeu24, hayeskhl
 *
 *  This is the Game!
 */

#ifndef PROJECT1_ANGRYSPARTY_H
#define PROJECT1_ANGRYSPARTY_H

#include <memory>

#include "ScoreBoard.h"
#include "Item.h"
#include "SpartyBird.h"
#include <wx/xml/xml.h>

class Item;
class SpartyBird;
class Physics;
class Level;
class AngrySpartyView;
class Visitor;


/// Maximum amount of time to allow for elapsed.
const double MaxElapsed = 0.050;

/**
* Base class for the game
*/
class AngrySparty {
private:

    /// Scale for the game
    double mScale = 0;
    /// x offset for the game
    double mXOffset = 0;
    /// y offset for the game
    double mYOffset = 0;

    /// pointer to physics object in game
    std::shared_ptr<Physics> mPhysics;

    /// pointer to the game's view
    AngrySpartyView *mAngrySpartyView;

    /// vector of pointers to the levels belonging to the game
    std::vector<std::shared_ptr<Level>> mLevels;

    /// vector of item pointers
    std::vector<std::shared_ptr<Item>> mItems;

    /// vector of sparty bird pointers
    std::vector<std::shared_ptr<SpartyBird>> mBirds;

    /// the games score board
    ScoreBoard mScoreBoard;

    /// item pointer for the grabbed item
    std::shared_ptr<Item> mGrabbedItem;

    /// The graphics object
    std::shared_ptr<wxGraphicsContext> mGraphics;

    /// width of the game
    double mWidth = 0;

    /// height of the game
    double mHeight = 0;

    /// The number level we are currently in.
    int mLevel;

    /// Bool indicates if we need to show welcome text
    bool mStart = true;

    /// Bool if the game just begin
    bool mStarted = true;

    /// Bool indicates if we complete the game
    bool mComplete = false;

    /// The timer that allows for animation
    wxTimer mTimer;

    /// Stopwatch used to measure elapsed time
    wxStopWatch mStopWatch;

    /// Stopwatch used to measure the welcome text
    wxStopWatch mStopWatchTwo;

    /// The last stopwatch time
    long mTime = 0;

    ///Index for bird vector
    int mCount = 0;

    /// True if the level was failed
    bool mFailed = false;

public:
    AngrySparty();
    void Load(const wxString &filename);
    void Update(double elapsed);
    void OnDraw(std::shared_ptr<wxGraphicsContext> graphics, int width, int height);
    void AddItem(std::shared_ptr<Item> item);
    void AddBird(std::shared_ptr<SpartyBird> bird);
    void GetLevelItemsNBirds(int level);
    void LoadLevel(int level);

    /**
    * Sets the view for the game
     * @param angrySpartyView the view class for the game
    */
    void SetGameView(AngrySpartyView* angrySpartyView){mAngrySpartyView = angrySpartyView;}

    std::shared_ptr<Item> HitTest(wxMouseEvent &event);
    void MoveToFront(std::shared_ptr<Item> item);

    void OnMouseMove(wxMouseEvent event);
    void OnLeftDown(wxMouseEvent event);

    /**
     * Clears any items or birds in the current level in the game
     */
    void Reset();

    /**
     * A getter for the scoreboard
     * @return Scoreboard of the game
     */
    ScoreBoard* GetScoreBoard(){return &mScoreBoard;}

    int NumDeadFoes();

    /**
     * Increments the count of dead birds
     * as to index into the mBirds and
     * keep track of how many birds are left
     */
    void IncrementCount()
    {
        mCount++;
    }


    /**
     * Gets the number of items for the playing level in the game for testing purpose
     * @return size of mItems
     */
    int GetNumItems(){ return mItems.size(); }

    /**
     * Gets the number of birds for the playing level in the game for testing purpose
     * @return size of mBirds
     */
    int GetNumBirds(){ return mBirds.size(); }

    int NumFoes();

    /**
     * Sets mLevel to current level
     * @param level
     */
    void SetLevel(int level){mLevel = level;}

    void Accept(Visitor* visitor);

    /**
     * Gets the value of mMovable of the first bird in mBirds for testing purpose
     * @return true if dynamic, false if static
     */
     bool GetMoveable(){ return mBirds[0]->GetMoveable(); }

     /**
      * Sets the location of the bird for testing purpose
      * @param i index of the bird in mBirds
      * @param x x position of the bird
      * @param y y position of the bird
      */
     void SetBirdLocation(int i, double x, double y){ mBirds[i]->SetLocation(x, y); }

     /**
      * Gets the location of the bird for testing purpose
      * @param i index of the bird in mBirds
      * @return (x position, y position)
      */
     std::tuple<double, double> GetBirdLocation(int i){ return std::make_pair(mBirds[i]->GetX(), mBirds[i]->GetY());}

};





#endif //PROJECT1_ANGRYSPARTY_H
