/**
 * @file AngrySparty.cpp
 * @author ahmedna8, chenan12, parkeu24, hayeskhl
 */

#include "pch.h"
#include "AngrySparty.h"
#include "Consts.h"
#include <wx/graphics.h>
#include <wx/dc.h>
#include "Background.h"
#include "Slingshot.h"
#include "Goalpost.h"
#include "Poly.h"
#include "GruffSpartyBird.h"
#include "Physics.h"
#include "Level.h"
#include <wx/dcbuffer.h>
#include <wx/dc.h>
#include "DebugDraw.h"
#include "FoeDieCount.h"
#include "Visitor.h"
#include "TntBlock.h"


using namespace std;

/**
 * Constructor
 */
AngrySparty::AngrySparty()
{

}

/**
 * Handle drawing the game on the screen including all subsystems.
 * @param graphics Graphics context to draw on
 * @param width Width of the window in pixels
 * @param height Height of the window in pixels
 */
void AngrySparty::OnDraw(std::shared_ptr<wxGraphicsContext> graphics, int width, int height)
{

    wxBrush background(*wxBLACK);
    graphics->SetBrush(background);
    graphics->DrawRectangle(0, 0, width, height);

    graphics->PushState();

    // Get the playing area size in centimeters

    //
    // Automatic Scaling
    // We use CM display units instead of meters
    // because a 1-meter wide line is very wide
    //

    auto scaleX = double(height) / double(mHeight);
    auto scaleY = double(width) / double(mWidth);
    mScale = scaleX < scaleY ? scaleX : scaleY;
    graphics->Scale(mScale, -mScale);


    // Determine the virtual size in cm
    auto virtualWidth = (double)width / mScale;
    auto virtualHeight = (double)height / mScale;

    // And the offset to the middle of the screen
    mXOffset = virtualWidth / 2.0;
    mYOffset = -(virtualHeight - mHeight) / 2.0 - mHeight;

    graphics->Translate(mXOffset, mYOffset);

    //
    // From here we are dealing with centimeter pixels
    // and Y up being increase values
    //
    // INSERT YOUR DRAWING CODE HERE

    mGraphics = graphics;

    for(auto item : mItems)
    {
        item->Draw(graphics);

    }
// we have can same vector for birds and item
    for(auto birds : mBirds)
    {
        birds->Draw(graphics);
    }


    for(auto items : mItems)
    {
        if(mCount < mBirds.size())
        {
            items->SetBird(mBirds[mCount]);
        }
        items->Draw2(graphics);
    }

    b2World* world = mPhysics->GetWorld();

    DebugDraw debugDraw(graphics);
    debugDraw.SetFlags(b2Draw::e_shapeBit | b2Draw::e_centerOfMassBit);
    world->SetDebugDraw(&debugDraw);
    world->DebugDraw();

    mScoreBoard.Display(graphics,scaleX,scaleY);


    if (mStarted)
    {
        mStopWatch.Pause();
        mStopWatchTwo.Start();

        while(mStopWatchTwo.Time() < 800)
        {

        }
        mStopWatch.Resume();
        mStarted = FALSE;
        mStopWatchTwo.Pause();
    }

    graphics->Scale(1, -1);	// Negate Y

    wxFont font2(84,
            wxFONTFAMILY_SWISS,
            wxFONTSTYLE_NORMAL,
            wxFONTWEIGHT_BOLD);

    wxColour color (0, 0, 150);

    graphics->SetFont(font2, color);


    string level = to_string(mLevel);

    if (mStart)
    {

        mStarted = TRUE;
        mStart = FALSE;
        wxString startMsg = L"Level " + level + " Begin";
        if (mLevel == 0)
        {
            graphics->DrawText(startMsg, -width/2.5, -height/1.5);
        }
        else if (mLevel == 1)
        {
            graphics->DrawText(startMsg, -width/2.5, -height/1.5);
        }
        else if (mLevel == 2)
        {
            graphics->DrawText(startMsg, -width/2.5, -height/1.5);
        }
        else
        {
            graphics->DrawText(startMsg,  -width/2.5, -height/1.5);
        }
        //SetStarted(true);
        //SetStart(true);
    }

    if(mComplete)
    {
        if(mFailed)
        {
            graphics->DrawText(L"Level Failed!",  -width/2.5, -height/1.5);
            mFailed = false;

            mStarted = TRUE;
            mStart = TRUE;

            GetScoreBoard()->SetScoreToZero();
            LoadLevel(mLevel);
            mCount = 0;


        }
        else
        {
            graphics->DrawText(L"Level Complete!",  -width/2.5, -height/1.5);

            mStarted = TRUE;
            mStart = TRUE;

            if (mLevel < 3)
            {
                mLevel++;
            }

            GetScoreBoard()->IncreaseLevelScore();
            GetScoreBoard()->SetScoreToZero();
            LoadLevel(mLevel);
            mCount = 0;
        }

    }



    graphics->PopState();


}

/**
* Load the AngrySparty from a .xml file
 *
 * Opens the XML file and reads the nodes, creating items as appropriate
 *
 * @param filename The filename of the file to load the AngrySparty Level from
*/
void AngrySparty::Load(const wxString &filename)
{
    shared_ptr<Level> level = make_shared<Level>();

    wxXmlDocument xmlDoc;

    if(!xmlDoc.Load(filename))
    {
        wxMessageBox(L"Unable to load AngrySparty Level file");
        return;
    }

    // Get the XML document root node
    auto root = xmlDoc.GetRoot();

    double w,h;

    root->GetAttribute(L"width", L"0").ToDouble(&w);
    root->GetAttribute(L"height", L"0").ToDouble(&h);

    double width = w * Consts::MtoCM;
    double height = h * Consts::MtoCM;

    level->SetLevelWidth(width);
    level->SetLevelHeight(height);

    // Traverse the children (linked list) of the root
    // node of the XML document in memory

    auto child = root->GetChildren();
    for ( ; child; child = child->GetNext())
    {
        auto name = child->GetName();
        if (name == L"items")
        {
            auto node = child->GetChildren();
            for(;node; node = node->GetNext())
            {
                level->XmlItem(this, node);
            }
        }
        else if(name == L"angry")
        {
            level->XmlBird(this, child);
        }
    }

    mLevels.push_back(level);
}

/**  Handle updates for animation
* @param elapsed The time since the last update
*/
void AngrySparty::Update(double elapsed)
{
    mTime+=elapsed;
    mPhysics->Update(elapsed);

    int deadFoes = NumDeadFoes();
    int totalFoes = NumFoes();

    if (mCount == mBirds.size())
    {
        mFailed = true;
        mComplete = true;
    }

    else if(deadFoes == totalFoes)
    {
        mComplete = TRUE;
    }
}

/** Loads the levels
* @param level the level number to be loaded
*/
void AngrySparty::LoadLevel(int level)
{
    Reset();
    mCount = 0;
    mLevel = level;
    GetLevelItemsNBirds(level);
    mStart = TRUE;

    GetScoreBoard()->SetScoreToZero();

    mPhysics = std::make_shared<Physics>(b2Vec2(mWidth,mHeight));

    for(auto items : mItems)
    {
        items->InstallPhysics(mPhysics);
    }

    for(auto birds : mBirds)
    {
        birds->InstallPhysics(mPhysics);
    }


}

/**
 * Sets mItems for the game to the mItems associated with the current level
 * @param item item being added to mItems
 */
void AngrySparty::AddItem(std::shared_ptr<Item> item)
{
    mItems.push_back(item);
}

/**
 * Sets mBirds for the game to the mBirds associated with the current level
 * @param bird bird being added to mBirds
 */
void AngrySparty::AddBird(std::shared_ptr<SpartyBird> bird)
{
    mBirds.push_back(bird);
}

/**
 * Sets the mItems and mBirds for the game to the mItems associated with the current level
 * @param level current level of the game
 */
void AngrySparty::GetLevelItemsNBirds(int level)
{
    shared_ptr<Level> currLevel = mLevels[level];
    currLevel->Destroy();
    currLevel->Install(this);
    mWidth = currLevel->GetLevelWidth();
    mHeight = currLevel->GetLevelHeight();
}

/**
 * Test an x,y click location to see if it clicked
 * on some item in the game.
 * @param event
 * @returns Pointer to item we clicked on or nullptr if none.
*/
std::shared_ptr<Item> AngrySparty::HitTest(wxMouseEvent &event)
{
    auto x = (event.m_x / mScale - mXOffset) / Consts::MtoCM;
    auto y = (event.m_y / -mScale - mYOffset) / Consts::MtoCM;
    for (auto item : mItems)
    {

        if (item->HitTest(x, y))
        {
            return item;
        }
    }

    return nullptr;
}

/**
 * Moves sparty to end when it's clicked on
 * on some item in the game.
 * @param item X location in pixels
*/
//this moves the bird to the last bird in mBirds, which may help us be able to delete it at the end
void AngrySparty::MoveToFront(std::shared_ptr<Item> item)
{
    auto loc = find(begin(mItems), end(mItems), item);
    if (loc != end(mItems))
    {
        mItems.erase(loc);
    }
    mItems.push_back(item);
}

/** Handles what happens when a left click happens
* @param event the mouse event on left click
*/
void AngrySparty::OnLeftDown(wxMouseEvent event)
{
    mGrabbedItem = HitTest(event);
    //rn mGrabbedItem is equal to nullptr
    if (mGrabbedItem != nullptr)
    {
        //maybe here is where I move the bird to the back of the vector
        //then we'll know to delete the last item in the vector when it's launched
        MoveToFront(mGrabbedItem);
    }
}

/** Handles what happens when the mouse moves
* @param event the mouse event on mouse move
*/

void AngrySparty::OnMouseMove(wxMouseEvent event)
{

    auto x = (event.GetX() / mScale - mXOffset) / Consts::MtoCM;
    auto y = (event.GetY() / -mScale - mYOffset) / Consts::MtoCM;

    b2Vec2 mouseLocation = b2Vec2(x, y);

    if (mGrabbedItem != nullptr)
    {
        // If an item is being moved, we only continue to
        // move it while the left button is down.
        if (event.LeftIsDown())
        {
            mGrabbedItem->SetBirdLocation(x, y);
        }
        else
        {
            // When the left button is released, we release the
            // item.
            mGrabbedItem->SetDynamic(mPhysics);
            mGrabbedItem = nullptr;
            //when the left button is released, we want it to launch here
        }
    }
}

/**
 * Accepts a visitor
 * @param visitor the visitor to accept
 */
void AngrySparty::Accept(Visitor* visitor)
{
    for (auto item : mItems)
    {
        item->Accept(visitor);
    }
}

/**
 * Gives us the number of dead foes
 * @return number of dead foes
 */
int AngrySparty::NumDeadFoes()
{
    FoeDieCount visitor;
    Accept(&visitor);
    return visitor.GetNumFoes();
}

/**
 * Gives us the total number of foes in a level
 * @return total foes in level
 */
int AngrySparty::NumFoes()
{
    FoeDieCount visitor;
    Accept(&visitor);
    return visitor.TotalFoes();
}

/**
 * Resets Everything to default values and clears vectors
*/

void AngrySparty::Reset()
{

    for(auto item : mItems)
    {
       item->SetLoadedFalse();
       item->AliveAgain();
       item->PrimeTnt();
    }
    mComplete = FALSE;

    FoeDieCount visitor;
    Accept(&visitor);

    visitor.ResetDeadFoes();
    visitor.ResetFoeCount();

    mItems.clear();
    mBirds.clear();
}

