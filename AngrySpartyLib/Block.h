/**
 * @file Block.h
 * @author hayeskhl, parkeu24, chenan12
 *
 *
 */

#ifndef ANGRYSPARTY_BLOCK_H
#define ANGRYSPARTY_BLOCK_H

#include "Item.h"
#include <memory>

/**
* Class for block in the game
*/
class Block : public Item {
private:
    /// The block image file
    std::wstring mBlockImageFile;

    /// The underlying block image
    std::shared_ptr<wxImage> mBlockImage;

    /// The bitmap we can display for this block
    std::shared_ptr<wxBitmap> mBlockBitmap;

    /// The physics object installed into block
    std::shared_ptr<Physics> mPhysics;


    /// Block attribute to be loaded from the XML file
    double mWidth; ///< width of the block
    double mHeight; ///< height of the block
    b2Vec2 mSize;   ///< size of the block
    b2Vec2 mPosition;   ///< position of the block
    double mAngle;  ///< angle of the block
    double mFriction;   ///< friction of the block
    double mRestitution; ///< restitution of the block
    double mDensity;    ///< density of the block
    std::wstring mType; ///< Type of the block

    /// true if block is static 
    /// or false if block is dynamic
    bool mStatic; 

    /// tells to repeat x for the sand/ground block  
    int mRepeatX;

public:
    /// Default constructor (disabled)
    Block() = delete;

    /// Copy constructor
    Block(const Block &) = delete;

    /// Assignment operator
    void operator=(const Block &) = delete;

    Block(AngrySparty* sparty, wxXmlNode *node);

    void XmlLoad(wxXmlNode* node) override;

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void InstallPhysics(std::shared_ptr<Physics> physics) override;

    void SetImage(const std::wstring& file) override;

};

#endif //ANGRYSPARTY_BLOCK_H
