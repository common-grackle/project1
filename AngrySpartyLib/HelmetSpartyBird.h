/**
 * @file HelmetSpartyBird.h
 * @author hayeskhl
 *
 *
 */

#ifndef ANGRYSPARTY_HELMETSPARTYBIRD_H
#define ANGRYSPARTY_HELMETSPARTYBIRD_H

#include "SpartyBird.h"
#include "Visitor.h"

/**
* Class for the helmet-sparty bird in the game
*/
class HelmetSpartyBird : public SpartyBird{
private:
    /// The helmet image file
    std::wstring mHelmetImageFile;

    /// The underlying sparty bird image
    std::unique_ptr<wxImage> mHelmetImage;

    /// The bitmap we can display for this slingshot
    std::shared_ptr<wxBitmap> mHelmetBitmap;

    /// radius of the bird
    double mRadius;

    /// velocity of the bird
    double mVelocity;


public:
    HelmetSpartyBird(AngrySparty *sparty, wxXmlNode *node);

    ///  Default constructor (disabled)
    HelmetSpartyBird() = delete;

    ///  Copy constructor (disabled)
    HelmetSpartyBird(const HelmetSpartyBird &) = delete;

    void XmlLoad(wxXmlNode* node) override;

    void SetImage(const std::wstring& file) override;

    double GetVelocity() override;

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */

    void Accept(Visitor* visitor) override { visitor->VisitHelmetSpartyBird(this); }
};

#endif //ANGRYSPARTY_HELMETSPARTYBIRD_H
