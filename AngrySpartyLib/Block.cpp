/**
 * @file Block.cpp
 * @author hayeskhl, parkeu24, chenan12
 */

#include "pch.h"
#include "Block.h"
#include "Physics.h"
#include "AngrySparty.h"
#include <wx/graphics.h>
#include "Consts.h"
#include "DebugDraw.h"

using namespace std;

/**
 * Constructor
 * @param sparty game this block is a member of
 * @param node the node that this block comes from
 */
Block::Block(AngrySparty* sparty, wxXmlNode *node) : Item(sparty)
{
    XmlLoad(node);
    SetImage(mBlockImageFile);
}

/** 
* Sets the image for the the block
* @param file the image file to use for the block
*/
void Block::SetImage(const std::wstring& file)
{
    wstring filename = L"images/" + file;
    mBlockImage = make_shared<wxImage>(filename, wxBITMAP_TYPE_ANY);
    mBlockBitmap = make_shared<wxBitmap>(*mBlockImage);

    mBlockImageFile = file;
}

/**
* brief Load the attributes for an item node.
* @param node The Xml node we are loading the item from
*/
void Block::XmlLoad(wxXmlNode* node)
{
    // Loaded x and y position of the item
    Item::XmlLoad(node);

    node->GetAttribute(L"width", L"0").ToDouble(&mWidth);
    node->GetAttribute(L"height", L"0").ToDouble(&mHeight);
    node->GetAttribute(L"angle", L"0").ToDouble(&mAngle);
    node->GetAttribute(L"friction", L"0.5").ToDouble(&mFriction);
    node->GetAttribute(L"restitution", L"0.5").ToDouble(&mRestitution);
    node->GetAttribute(L"density", L"1").ToDouble(&mDensity);
    node->GetAttribute(L"repeat-x", L"1").ToInt(&mRepeatX);
    if(node->GetAttribute(L"type", L"FALSE") == "static")
    {
        mStatic = TRUE;
    }
    else
    {
        mStatic = FALSE;
    }

    mType = node->GetAttribute(L"type");
    mBlockImageFile = node->GetAttribute(L"image");

    SetImage(mBlockImageFile);
}

/**
* Installs the physics into the block 
* @param physics the physics object being installed
* into the block
*/
void Block::InstallPhysics(std::shared_ptr<Physics> physics)
{
    mPhysics = physics;
    b2World* world = mPhysics->GetWorld();


    mSize = b2Vec2(mWidth , mHeight);
    mPosition = b2Vec2(GetX() , GetY());

    // Create the box
    b2PolygonShape box;
    box.SetAsBox(mSize.x/2, mSize.y/2);

    // Create the body definition
    b2BodyDef bodyDefinition;
    bodyDefinition.position = mPosition;
    bodyDefinition.angle = mAngle * Consts::DtoR;
    bodyDefinition.type = mStatic ? b2_staticBody : b2_dynamicBody;
    auto body = world->CreateBody(&bodyDefinition);

    if(mStatic)
    {
        body->CreateFixture(&box, 0.0f);
    }
    else
    {
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &box;
        fixtureDef.density = (float)mDensity;
        fixtureDef.friction = (float)mFriction;
        fixtureDef.restitution = (float)mRestitution;

        body->CreateFixture(&fixtureDef);
    }
    SetBody(body);



}

/**
* Draws the block object
* @param graphics graphics object to draw the block item
*/
void Block::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    graphics->PushState();

    auto body = GetBody();


    auto position = body->GetPosition();
    auto angle = body->GetAngle();

    graphics->Translate(position.x * Consts::MtoCM,
            position.y * Consts::MtoCM);
    graphics->Rotate(angle);



//     Make this is left side of the rectangle

    double x = -(mSize.x / 2) * Consts::MtoCM;

    // And the top
    double y = (mSize.y / 2) * Consts::MtoCM;

    // The width of each repeated block
    double xw = (mSize.x) / mRepeatX * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mBlockBitmap;


    graphics->Translate(0, y);
    graphics->Scale(1, -1);

    for(int ix=0; ix<mRepeatX;  ix++)
    {
        graphics->DrawBitmap(*bitmap,x,0,xw, mSize.y * Consts::MtoCM);

        x += xw;
    }

    graphics->PopState();
}

