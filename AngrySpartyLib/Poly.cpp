/**
 * @file Poly.cpp
 * @author hayeskhl, chenan12, parkeu24
 */

#include "pch.h"
#include "Poly.h"
#include "AngrySparty.h"
#include "Consts.h"
#include <wx/graphics.h>
#include "Physics.h"

using namespace std;

/**
 * Constructor
 * @param sparty game this block is a member of
 * @param node the node this poly is from
 */
Poly::Poly(AngrySparty* sparty, wxXmlNode *node) : Item(sparty)
{
    XmlLoad(node);
    SetImage(mPolyImageFile);
}

/**
* Sets the poly image
* @param file the image file to set the poly as
*/
void Poly::SetImage(const std::wstring& file)
{
    wstring filename = L"images/" + file;
    mPolyImage = make_shared<wxImage>(filename, wxBITMAP_TYPE_ANY);
    mPolyBitmap = make_shared<wxBitmap>(*mPolyImage);

    mPolyImageFile = file;
}

/**
* brief Load the attributes for an item node.
* @param node The Xml node we are loading the item from
*/
void Poly::XmlLoad(wxXmlNode* node)
{
    // Loaded x and y position of the item
    //    node->GetAttribute(L"x", L"0").ToDouble(&mX);
    //    node->GetAttribute(L"y", L"0").ToDouble(&mY);
    Item::XmlLoad(node);

    // no such attribute for poly in xml file
     node->GetAttribute(L"width", L"0").ToDouble(&mPolyWidth);
     node->GetAttribute(L"height", L"0").ToDouble(&mPolyHeight);
     node->GetAttribute(L"angle", L"0").ToDouble(&mPolyAngle);
     node->GetAttribute(L"friction", L"0.5").ToDouble(&mPolyFriction);
     node->GetAttribute(L"restitution", L"0.5").ToDouble(&mPolyRestitution);
     node->GetAttribute(L"density", L"1").ToDouble(&mPolyDensity);
     node->GetAttribute(L"repeat-x", L"0").ToInt(&mPolyRepeatX);
     if(node->GetAttribute(L"type", L"FALSE") == "static")
     {
         mPolyStatic = TRUE;
     }
     else
     {
         mPolyStatic = FALSE;
     }
     auto child = node->GetChildren();

     if (child->GetName() == 'v')
     {
         for (; child; child = child->GetNext())
         {

             double x, y;
             child->GetAttribute(L"x", L"0").ToDouble(&x);
             child->GetAttribute(L"y", L"0").ToDouble(&y);

             mVertices.push_back(b2Vec2(x, y));
         }

     }


    // mType = node->GetAttribute(L"type");
    mPolyImageFile = node->GetAttribute(L"image");
}

/**
* Installs the physics into the poly 
* @param physics the physics object being installed
* into the poly
*/
void Poly::InstallPhysics(std::shared_ptr<Physics> physics)
{
    mPhysics = physics;
    b2World* world = mPhysics->GetWorld();

    mPolyPosition = b2Vec2(GetX() , GetY());
    mPolySize = b2Vec2(mPolyWidth, mPolyHeight);

    // Create the poly
    b2PolygonShape poly;
    poly.Set(&mVertices[0], mVertices.size());

    // Create the body definition
    b2BodyDef bodyDefinition;
    bodyDefinition.position = mPolyPosition;
    bodyDefinition.angle = mPolyAngle * Consts::DtoR;
    bodyDefinition.type = mPolyStatic ? b2_staticBody : b2_dynamicBody;
    auto body = world->CreateBody(&bodyDefinition);

    if(mPolyStatic)
    {
        body->CreateFixture(&poly, 0.0f);
    }
    else
    {
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &poly;
        fixtureDef.density = (float)mPolyDensity;
        fixtureDef.friction = (float)mPolyFriction;
        fixtureDef.restitution = (float)mPolyRestitution;

        body->CreateFixture(&fixtureDef);
    }
    SetBody(body);
}

/**
* Draws the poly object
* @param graphics graphics object to draw the poly item
*/
void Poly::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto body = GetBody();

    auto position = body->GetPosition();
    auto angle = body->GetAngle();

    // Find the minimum and maximum x/y values
    b2Vec2 minimums = mVertices[0];
    b2Vec2 maximums = mVertices[0];
    for(auto v : mVertices)
    {
        minimums.x = fmin(minimums.x, v.x);
        minimums.y = fmin(minimums.y, v.y);
        maximums.x = fmax(maximums.x, v.x);
        maximums.y = fmax(maximums.y, v.y);
    }

    auto size = maximums - minimums;

    auto x = position.x * Consts::MtoCM;
    auto y = position.y * Consts::MtoCM;

    graphics->PushState();
    graphics->Translate(x, y);
    graphics->Rotate(angle);

    std::shared_ptr<wxBitmap> bitmap = mPolyBitmap;

    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap,
            minimums.x * Consts::MtoCM,
            minimums.y * Consts::MtoCM,
            size.x * Consts::MtoCM,
            size.y * Consts::MtoCM);

    graphics->PopState();
}


