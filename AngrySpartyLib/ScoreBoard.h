/**
 * @file ScoreBoard.h
 * @author parkeu24
 *
 * Class for the score board for the game
 */

#ifndef ANGRYSPARTY_SCOREBOARD_H
#define ANGRYSPARTY_SCOREBOARD_H

/**
 * Class for the score boarad for the AngrySparty game
 */
class ScoreBoard {
private:
    /// Score Number
    int mScore = 0;
    /// Level number
    int mLevel = 0;

public:
    void Display(std::shared_ptr<wxGraphicsContext> graphics,double x, double y);

    /**
     * Setter to increase score
     */
    void IncreaseScore() { mScore += 100;}

    /**
     * Setter to increase Levels
     */
    void IncreaseLevelScore() { mLevel +=mScore;}

    /**
     * Sets the score back to 0
     */
    void SetScoreToZero(){mScore=0;}

    /**
 * Getter for the current score in the ScoreBoard
 *
 * @return int mScore
 */
    int GetScore() const { return mScore; }



    std::vector<int> GetScores();
};

#endif //ANGRYSPARTY_SCOREBOARD_H
