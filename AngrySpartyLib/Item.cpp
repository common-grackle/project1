/**
 * @file Item.cpp
 * @author hayeskhl, parkeu24
 */

#include "pch.h"
#include "Item.h"
#include "Consts.h"

using namespace std;

/**
 * Constructor
 * @param sparty The game this item is a member of
 */
Item::Item(AngrySparty *sparty)
{
    mAngrySparty=sparty;
}

/**
 * Destructor
 */
Item::~Item()
{

}

/**
 *  Set the image file to draw
 * @param file The base filename. Blank files are allowed
 */
 void Item::SetImage(const std::wstring &file)
{

}

/**
 * Load the attributes for an item node.
 *
 * This is the  base class version that loads the attributes
 * common to all items. Override this to load custom attributes
 * for specific items.
 *
 * @param node The Xml node we are loading the item from
 */
void Item::XmlLoad(wxXmlNode *node)
{
    node->GetAttribute(L"x", L"0").ToDouble(&mX);
    node->GetAttribute(L"y", L"0").ToDouble(&mY);
}





