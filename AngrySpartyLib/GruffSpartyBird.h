/**
 * @file GruffSpartyBird.h
 * @author hayeskhl
 *
 *
 */

#ifndef ANGRYSPARTY_GRUFFSPARTYBIRD_H
#define ANGRYSPARTY_GRUFFSPARTYBIRD_H

#include "SpartyBird.h"
#include "Visitor.h"

/**
* Class for the gruff-sparty bird in the game
*/
class GruffSpartyBird : public SpartyBird {
private:
    /// The Gruff image file
    std::wstring mGruffImageFile;

    /// The underlying sparty bird image
    std::unique_ptr<wxImage> mGruffImage;

    /// The bitmap we can display for this slingshot
    std::shared_ptr<wxBitmap> mGruffBitmap;

    /// velocity factor
    double mVelocity;

    /// radius of the bird
    double mRadius;


public:
    GruffSpartyBird(AngrySparty *sparty, wxXmlNode *node);

    ///  Default constructor (disabled)
    GruffSpartyBird() = delete;

    ///  Copy constructor (disabled)
    GruffSpartyBird(const GruffSpartyBird &) = delete;

    void SetImage(const std::wstring& file) override;

    void XmlLoad(wxXmlNode* node) override;

    double GetVelocity() override;

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */

    void Accept(Visitor* visitor) override { visitor->VisitGruffSpartyBird(this); }

};

#endif //ANGRYSPARTY_GRUFFSPARTYBIRD_H
