/**
 * @file Visitor.h
 * @author NEW
 *
 *
 */

#ifndef ANGRYSPARTY_VISITOR_H
#define ANGRYSPARTY_VISITOR_H


class Item;
class Block;
class Poly;
class Goalpost;
class Slingshot;
class Background;
class Physics;
class SpartyBird;
class ScoreBoard;
class Foe;
class Level;
class HelmetSpartyBird;
class GruffSpartyBird;

/**
* visitor class for the angry sparty game
*/
class Visitor {
private:

protected:
    Visitor() { }

public:
    virtual ~Visitor() { }

    /**
     * Visits physics object
     * @param physics object to visit
     */
    virtual void VisitPhysics(Physics* physics) { }

    /**
     * Visits Block object
     * @param block object to visit
     */
    virtual void VisitBlock(Block* block) { }

    /**
     * Visits poly object
     * @param poly object to visit
     */
    virtual void VisitPoly(Poly* poly) { }

    /**
     * Visits slingshot object
     * @param slingshot object to visit
     */
    virtual void VisitSlingshot(Slingshot* slingshot) { }

    /**
     * Visits goalpost object
     * @param goalpost object to visit
     */
    virtual void VisitGoalPost(Goalpost* goalpost) { }

    /**
     * Visits background object
     * @param background object to visit
     */
    virtual void VisitBackground(Background* background) { }

    /**
     * Visits scoreboard object
     * @param scoreBoard object to visit
     */
    virtual void VisitScoreBoard(ScoreBoard* scoreBoard) { }

    /**
     * Visits foe object
     * @param foe object to visit
     */
    virtual void VisitFoe(Foe* foe) {}

    /**
     * Visits Level object
     * @param level object to visit
     */
    virtual void VisitLevel(Level* level) {}

    /**
     * Visit a HelmetSpartyBird object
     * @param spartybird HelmetSpartyBird we are visiting
     */
    virtual void VisitHelmetSpartyBird(HelmetSpartyBird* spartybird) { }

    /**
     * Visit a GruffSpartyBird object
     * @param spartybird GruffSpartyBird we are visiting
     */
    virtual void VisitGruffSpartyBird(GruffSpartyBird* spartybird) { }

};

#endif //ANGRYSPARTY_VISITOR_H
