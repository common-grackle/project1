/**
 * @file Goalpost.cpp
 * @author ahmedna8
 */

#include "pch.h"
#include "Goalpost.h"
#include "Consts.h"
#include <wx/graphics.h>
#include "Physics.h"


using namespace std;

/// Base filename for the goalpost image
const wstring GoalpostsBaseName = L"images/goalposts.png";

/// filename for the goalpost front image
const wstring GoalpostsFrontName = L"images/goalposts-front.png";

/// Size of the goalpost image in meters
const b2Vec2 GoalpostsSize = b2Vec2(1, 2.649);

/// Back band attachment point
const b2Vec2 GoalpostsBandAttachBack = b2Vec2(-0.42f, 2.3f);

/// Front band attachment point
const b2Vec2 GoalpostsBandAttachFront = b2Vec2(0.34f, 2.32f);

/// Maximum amount the slingshot can be pulled in meters
const double GoalpostsMaximumPull = 2;

/// Pull angles from -pi to this value are allowed
const double GoalpostsMaximumNegativePullAngle = -1.7;

/// Pull angles from +pi to this value are allowed
const double GoalpostsMinimumPositivePullAngle = 2.42;

/// Width of the slingshot band in centimeters
const int GoalpostsBandWidth = 15;

/// The slingshot band colour
const wxColour GoalpostsBandColor = wxColour(55, 18, 1);

/**
 * Constructor
 * @param angrySparty game this goalpost is a member of
 * @param node the node this goalpost is from
 */
Goalpost::Goalpost(AngrySparty* angrySparty, wxXmlNode *node) : Item(angrySparty)
{
    XmlLoad(node);
    mGoalpostImage = make_shared<wxImage>(GoalpostsBaseName, wxBITMAP_TYPE_ANY);
    mGoalpostBitmap = make_shared<wxBitmap>(*mGoalpostImage);

    mGoalpostFrontImage = make_shared<wxImage>(GoalpostsFrontName, wxBITMAP_TYPE_ANY);
    mGoalpostFrontBitmap = make_shared<wxBitmap>(*mGoalpostFrontImage);
}

/**
 * Draw this Slingshot
 * @param graphics grpahics context to draw on
 */
void Goalpost::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{

    graphics->PushState();


    auto wid = GoalpostsSize.x * Consts::MtoCM;
    auto hit = GoalpostsSize.y * Consts::MtoCM;
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;


    std::shared_ptr<wxBitmap> bitmap = mGoalpostBitmap;

    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap, x - (wid/2), - y - hit,wid, hit);

    //draw band here

    wxPen slingshotPen(GoalpostsBandColor, GoalpostsBandWidth);
    graphics->SetPen(slingshotPen);


    graphics->PopState();


    if(!mDrawSling)
    {
        graphics->PushState();
        double x1 = (GoalpostsBandAttachBack.x* Consts::MtoCM) + x;
        double y1 = (GoalpostsBandAttachBack.y* Consts::MtoCM) +  y;
        double x2 = (GoalpostsBandAttachFront.x* Consts::MtoCM) + x;
        double y2 = (GoalpostsBandAttachFront.y* Consts::MtoCM) + y;
        graphics->StrokeLine(x1, y1, x2, y2);
        graphics->PopState();
    }
}

/**
* Sets the bird that is going to go in the goalpost slingshot
* @param spartyBird the bird to put in slingshot
*/
void Goalpost::SetBird(std::shared_ptr<SpartyBird> spartyBird)
{
    mCurrentBird = spartyBird;
}

/**
* Sets the background state of the goalpost
* @param graphics graphics object used to draw the background state
*/
void Goalpost::BackgroundState(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto wid = GoalpostsSize.x * Consts::MtoCM;
    auto hit = GoalpostsSize.y * Consts::MtoCM;
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;


    std::shared_ptr<wxBitmap> bitmap = mGoalpostBitmap;

    graphics->PushState();
    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap, x - (wid/2), - y - hit,wid, hit);
    graphics->PopState();
}

/**
* Draws the left band for holding the bird
* @param graphics the graphics object that draws the left band
*/
void Goalpost::LeftBandState(std::shared_ptr<wxGraphicsContext> graphics)
{
    graphics->PushState();


    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    //draw left band here


    wxPen slingshotPen(GoalpostsBandColor, GoalpostsBandWidth);
    graphics->SetPen(slingshotPen);


    double x1 = (GoalpostsBandAttachBack.x* Consts::MtoCM) + x;
    double y1 = (GoalpostsBandAttachBack.y* Consts::MtoCM) +  y;

    double x2 = mCurrentBird->GetX()*Consts::MtoCM;
    double y2 = mCurrentBird->GetY()*Consts::MtoCM;
    graphics->StrokeLine(x1, y1, x2, y2);

    graphics->PushState();

}

/**
* Sets the birds state for its relationship to the slingshot
*/
void Goalpost::BirdState()
{

    auto x = mX;
    auto y = mY;

    double x1 = (GoalpostsBandAttachBack.x) + x;
    double y1 = (GoalpostsBandAttachBack.y) +  y;
    double x2 = (GoalpostsBandAttachFront.x) + x;
    double y2 = (GoalpostsBandAttachFront.y) + y;



    double newX = (x1 + x2)/2;
    double newY = (y1 + y2)/2;

    mCurrentBird->SetLoadX(newX);
    mCurrentBird->SetLoadY(newY);

    mCurrentBird->SetBodyLocation(newX,newY);
}

/**
* Draws the right band for holding the bird
* @param graphics graphics object for drawing the right band
*/
void Goalpost::RightBandState(std::shared_ptr<wxGraphicsContext> graphics)
{

    graphics->PushState();

    //draw band here
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    wxPen slingshotPen(GoalpostsBandColor, GoalpostsBandWidth);
    graphics->SetPen(slingshotPen);

    double x1 = mCurrentBird->GetX()*Consts::MtoCM;
    double y1 = mCurrentBird->GetY()*Consts::MtoCM;

    double x2 = (GoalpostsBandAttachFront.x* Consts::MtoCM) + x;
    double y2 = (GoalpostsBandAttachFront.y* Consts::MtoCM) + y;

    graphics->StrokeLine(x1, y1, x2, y2);
    graphics->StrokeLine(x1, y1, x1 - 1, y1);
    graphics->PopState();

}

/**
* Redraws the slingshot after changing it to hold bird
* @param graphics graphics object to draw the goalpost with
*/
void Goalpost::FinalState(std::shared_ptr<wxGraphicsContext> graphics)
{
    graphics->PushState();

    auto wid = GoalpostsSize.x * Consts::MtoCM;
    auto hit = GoalpostsSize.y * Consts::MtoCM;
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mGoalpostFrontBitmap;

    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap, x - (wid/2), - y - hit,wid, hit);

    graphics->PopState();
}

/** Sets bird location
* @param x the x position
* @param y the y position
*/
void Goalpost::SetBirdLocation(double x, double y)
{
    b2Vec2 location = b2Vec2(x,y);

    double x1 = (GoalpostsBandAttachBack.x)+(mX);
    double y1 = (GoalpostsBandAttachBack.y)+(mY);
    double x2 = (GoalpostsBandAttachFront.x)+(mX);
    double y2 = (GoalpostsBandAttachFront.y)+(mY);

    b2Vec2 loadPoint = b2Vec2((x1+x2)/2,(y1+y2)/2);
    b2Vec2 toAngry = location - loadPoint;

    double theta = atan2(toAngry.y, toAngry.x);

    double bandLength = sqrt(pow(toAngry.x,2) + pow(toAngry.y,2));

    if (bandLength <= GoalpostsMaximumPull && ( (-M_PI <= theta && theta <=  GoalpostsMaximumNegativePullAngle) || (GoalpostsMinimumPositivePullAngle <= theta && theta <=  M_PI) ))
    {
        mCurrentBird->SetBodyLocation(x,y);
    }
    else
    {
        auto toAngryShortened = (GoalpostsMaximumPull/bandLength)*toAngry;
        auto newLocation = loadPoint + toAngryShortened;
        auto newToAngry = newLocation - loadPoint;
        double newTheta = atan2(newToAngry.y, newToAngry.x);
        double newBandLength = sqrt(pow(newToAngry.x,2) + pow(newToAngry.y,2));
        if (newBandLength <= GoalpostsMaximumPull && ( (-M_PI <= newTheta && newTheta <=  GoalpostsMaximumNegativePullAngle) || (GoalpostsMinimumPositivePullAngle <= newTheta && newTheta <=  M_PI) ))
        {
            mCurrentBird->SetBodyLocation(newLocation.x,newLocation.y);
        }

    }
}

/**
* Tests to see if hit (click) was successful
* @param x the x position of click
* @param y the y position of click
* @return bool true if hit was successful
* false otherwise
*/
bool Goalpost::HitTest(double x, double y)
{
    return mCurrentBird->HitTest(x,y);
}

/**
* Sets from static to dynamic
 * @param physics the physics object to install to bird
*/
void Goalpost::SetDynamic(std::shared_ptr<Physics> physics)
{
    mCurrentBird->SetMoveable(TRUE);
    mCurrentBird->InstallPhysics(physics);
    mDrawSling = FALSE;
    mLoaded = FALSE;
}

/**
* Loads the goalpost attributes from xml file
* @param node the xml node to load from
*/
void Goalpost::XmlLoad(wxXmlNode* node)
{
        // load x, and y
    node->GetAttribute(L"x", L"0").ToDouble(&mX);
    node->GetAttribute(L"y", L"0").ToDouble(&mY);
}

/**
 * Draws the goalpost a second time
 * @param graphics used to draw
 */
void Goalpost::Draw2(std::shared_ptr<wxGraphicsContext> graphics)
{
    if(mDrawSling)
    {
        LeftBandState(graphics);
    }
    b2Body* body = mCurrentBird->GetBody();
    if(!mLoaded && body->GetType() == b2_staticBody)
    {
        BirdState();
        mLoaded = TRUE;
        mDrawSling = TRUE;
    }
    if(mDrawSling)
    {
        RightBandState(graphics);
        FinalState(graphics);
    }
}