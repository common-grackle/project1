/**
 * @file Level.cpp
 * @author parkeu24
 */

#include "pch.h"
#include <wx/dc.h>
#include "Level.h"
#include "Item.h"
#include "Background.h"
#include "ScoreBoard.h"
#include "Slingshot.h"
#include "Goalpost.h"
#include "Block.h"
#include "Poly.h"
#include "Foe.h"
#include "GruffSpartyBird.h"
#include "HelmetSpartyBird.h"
#include "TntBlock.h"
using namespace std;

/**
 * Add an item to the game
 * @param item New item to add
 */
void Level::AddItem(std::shared_ptr<Item> item)
{
    mItems.push_back(item);
}


/**
 * Add a bird to the game
 * @param bird New bird to add
 */
void Level::AddBird(std::shared_ptr<SpartyBird> bird)
{
    double newX = bird->GetX();
    double newY = bird->GetY();
    for (auto i = mBirds.begin(); i != mBirds.end();  i++)
    {
        double dist = bird->DistanceTo(*i);
        if ( dist < 1 )
        {
            newX += bird->GetSpacing();

            bird->SetLocation(newX, newY);
        }
    }

    mBirds.push_back(bird);
}

/**
 * Handle a node of type item
 * @param angrySparty the game this level belongs to
 * @param node XML node
 */
void Level::XmlItem(AngrySparty* angrySparty, wxXmlNode *node)
{
    shared_ptr<Item> item;

    auto type = node->GetName();

    if (type == L"block")
    {
        item = make_shared<Block>(angrySparty, node);
    }
    else if (type == L"poly")
    {
        item = make_shared<Poly>(angrySparty, node);
    }
    else if (type == L"foe")
    {
        item = make_shared<Foe>(angrySparty, node);
    }
    else if (type == L"goalposts")
    {
        item = make_shared<Goalpost>(angrySparty, node);
    }
    else if (type == L"slingshot")
    {
        item = make_shared<Slingshot>(angrySparty, node);
    }
    else if (type == L"tnt")
    {
        item = make_shared<TntBlock>(angrySparty, node);
    }
    else if (type == L"background")
    {
        item = make_shared<Background>(angrySparty, node);
    }

    if (item != nullptr)
    {
        AddItem(item);
    }
}

/**
 * Handle a node of type spartybird
 * @param angrySparty the game this level belongs to
 * @param node XML node
 */
void Level::XmlBird(AngrySparty* angrySparty, wxXmlNode *node)
{
    shared_ptr<SpartyBird> bird;

    auto child = node->GetChildren();

    for (; child; child = child->GetNext()) {
        if (child->GetName()==L"gruff-sparty") {
            bird = make_shared<GruffSpartyBird>(angrySparty, node);
        }
        else if (child->GetName()==L"helmet-sparty") {
            bird = make_shared<HelmetSpartyBird>(angrySparty, node);
        }
        if (bird!=nullptr) {
            AddBird(bird);
        }
    }
}

/** 
* Installs the game into levels
* @param game The game used in installation
*/
void Level::Install(AngrySparty* game)
{
    game->Reset();

    for (auto item : mItems)
    {
        game->AddItem(item);
    }

    for (auto bird : mBirds)
    {
        game->AddBird(bird);
    }
}

/**
* Destroys all the newly updated mItems and mBirds (from played before) then sets back to original value
*/
void Level::Destroy()
{
    auto copyBirds = mBirds;
    mBirds.clear();
    for(auto bird : copyBirds)
    {
        bird->SetDefault();
        AddBird(bird);
    }
}

/**
 * Accepts a visitor to the level object
 * @param visitor that visits level object
 */
void Level::Accept(Visitor* visitor)
{
    for(auto item : mItems)
    {
        item->Accept(visitor);
    }
}
