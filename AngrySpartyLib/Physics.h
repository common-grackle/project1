/**
 * @file Physics.h
 * @author hayeskhl
 *
 *
 */

#ifndef ANGRYSPARTY_PHYSICS_H
#define ANGRYSPARTY_PHYSICS_H

class b2World;
class Item;

#include "Visitor.h"
#include "AngrySpartyView.h"
#include "Block.h"
#include "SpartyBird.h"


/**
 * Physics system for a level.
 */
class Physics{
private:
    /// The box2d world
    b2World mWorld;

    /// A ground reference object
    b2Body* mGround;

//    Block *mBlock;

    /// Scale we are drawing at
    double mScale = 1;

    /// X offset when we draw
    double mXOffset = 0;

    /// Y offset when we draw
    double mYOffset = 0;

    /// Mouse location
    b2Vec2 mMouseLocation;

    /// Size of the playing area in meters
    b2Vec2 mSize;


public:
    /// Constructor
    Physics(const b2Vec2& size);
    void Update(double elapsed);

    /**
    * Gets the world the physics belongs to
    * @return mWorld the world the physics belongs to
    */
    b2World* GetWorld() { return &mWorld; }

    /**
    * Accepts visitors to physics
    * @param visitor visits the physics object
    */
    void Accept(Visitor* visitor) { visitor->VisitPhysics(this); }
};

#endif //ANGRYSPARTY_PHYSICS_H
