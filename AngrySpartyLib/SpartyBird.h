/**
 * @file SpartyBird.h
 * @author hayeskhl
 *
 *
 */

#ifndef PROJECT1_SPARTYBIRD_H
#define PROJECT1_SPARTYBIRD_H

#include <string>
#include <memory>
#include <wx/xml/xml.h>
#include <wx/graphics.h>
#include "Visitor.h"


class Physics;
class b2Body;
class b2World;
class b2Fixture;
class b2Shape;
class AngrySparty;
class Visitor;
class Item;

/**
 * Sparty bird class
 */
class SpartyBird  {
private:
    /// The game this bird is contained to
    AngrySparty *mAngrySparty;

    /// pointer to the b2 world the bird belongs to
    b2World* mWorld;

    /// Item location in the game
    double mX;  ///< X location for the center of the item
    double mY;  ///< Y location for the center of the item

    /// original x from the xml file to be used for reloading a level
    double mOriginalX;
    /// original y from the xml file to be used for reloading a level
    double mOriginalY;
    /// original spacing from the xml file to be used for reloading a level
    double mOriginalSpacing;

    /// The image for this item
    std::shared_ptr<wxImage> mSpartyBirdImage;

    /// The bitmap for this item
    std::shared_ptr<wxBitmap> mSpartyBirdBitmap;

    /// The file for this item
    std::wstring mSpartyBirdFile;

    /// The physics system body
    b2Body *mBody = nullptr;

    /// Spacing between the birds
    double mSpacing;

    /// Radius of the birds
    double mRadius = 0.25;

    /// angle of the bird
    double mAngle;

    /// physics to install
    std::shared_ptr<Physics> mPhysics;

    /// determines if bird is static or dynamic
    bool mMoveable = FALSE;

    /// Loaded value of x
    double mLoadX;
    /// Loaded value of y
    double mLoadY;

    /// true if the bird has died
    bool mDead = false;

protected:
    SpartyBird(AngrySparty *sparty, wxXmlNode *node);

public:
    /// Default constructor (disabled)
    SpartyBird() = delete;

    virtual ~SpartyBird();

    /// Copy constructor
    SpartyBird(const SpartyBird &) = delete;

    /// Assignment operator
    void operator=(const SpartyBird &) = delete;


    /**
     * The X location of the bird
     * @return X location in pixels
     */
    double GetX() const { return mX; }

    /**
     * The Y location of the bird
     * @return Y location in pixels
     */
    double GetY() const { return mY; }

//    /**
//     * Set the bird location
//     * @param x X location in pixels
//     * @param y Y location in pixels
//     */
//    void SetLocation(double x, double y) { mX = x; mY = y; }

    virtual void XmlLoad(wxXmlNode* node);

//    void SetPos(b2Vec2 pos){mPosition = pos;};

    /**
     * Getter for the velocity of the diff types of birds
     *
     * @return double the velocity of the bird
     */
    virtual double GetVelocity() = 0;

//    /**  Test this sparty bird to see if it has been clicked on
//    * @param x X location on the bird to test
//    * @param y Y location on the bird to test
//    * @return true if clicked on */
//    virtual bool HitTest(int x, int y);

    virtual void SetImage(const std::wstring &file);

    void Draw(std::shared_ptr<wxGraphicsContext> graphics);

    /**
     * Gets the spacing between the birds
     * @return mSpacing the spacing between the birds
     */
    double GetSpacing(){ return mSpacing; }

    double DistanceTo(std::shared_ptr<SpartyBird> bird);

    void SetLocation(double x, double y);

    /** Gets the b2Body of the bird
    * @return mBody the b2Body of the bird
    */
    b2Body* GetBody(){return mBody;};

    /** Sets the physics body of the bird
    * @param body the body to set
    */
    void SetBody(b2Body* body){mBody = body;}

    void InstallPhysics(std::shared_ptr<Physics> physics);

    void SetBodyLocation(double x, double y);

    bool HitTest(double x, double y);

    /** Gets the angle of the bird
    * @return mAngle the angle of the bird
    */
    double GetAngle(){return mAngle;}

    /** Sets the bird as dynamic
    * @param moveable sets to dynamic
    */ 
    void SetMoveable(bool moveable) {mMoveable = moveable;};

    /**
     * Gets the value of mMoveable associated with the bird
     * @return mMoveable
     */
    bool GetMoveable() { return mMoveable; }

    /**
     * Setter of the loaded initial x position of bird
     * @param x the x position to set the initial to
     */
    void SetLoadX(double x){mLoadX = x;}

    /**
     * Setter of the loaded initial y position of bird
     * @param y the y position to set the initial to
     */
    void SetLoadY(double y){mLoadY = y;}

    /**
     * Tells if bird is dead
     * @return bool true if bird is dead,
     * false otherwise
     */
    bool Dead(){return mDead;}

    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    virtual void Accept(Visitor* visitor){}

    void SetDefault();
};

#endif //PROJECT1_SPARTYBIRD_H
