/**
 * @file ScoreBoard.cpp
 * @author parkeu24
 */

#include "pch.h"
#include "ScoreBoard.h"
#include <wx/graphics.h>
#include "Consts.h"

/**
 * Displays the scoreboard on the screen
 * @param graphics graphics context to draw scoreboard on
 * @param x the width of scoreboard
 * @param y the height of scoreboard
 */
void ScoreBoard::Display(std::shared_ptr<wxGraphicsContext> graphics,double x, double y)
{
    graphics->PushState();
    const wxFont Nfont(wxSize(24,60),
            wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD );
    graphics->SetFont(Nfont, wxColour(999,0,0));
    graphics->Scale(1, -1);	// Negate Y
    graphics->DrawText(wxString::Format(wxT("%i"),mScore), x+605,-y-800);
    graphics->DrawText(wxString::Format(wxT("%i"),mLevel), -x-700,-y-800);
    //graphics->DrawText(wxString::Format(wxT("%i"),mLevel), playingAreaSize.x / 2 - wid - 20, -playingAreaSize.y +10)
    graphics->PopState();
}

/**
 * Gets Scores that are currently on scoreboard
 * @return vector containing [Level, Score]
 */
std::vector<int> ScoreBoard::GetScores()
{
    std::vector<int> results;
    results.push_back(mScore);
    results.push_back(mLevel);

    return results;
}
