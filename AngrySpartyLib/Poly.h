/**
 * @file Poly.h
 * @author hayeskhl, chenan12, parkeu24
 *
 *
 */

#ifndef ANGRYSPARTY_POLY_H
#define ANGRYSPARTY_POLY_H

#include "Item.h"
#include <memory>

/**
* Class for the poly item in the game
*/
class Poly : public Item {
private:
    /// The poly image file
    std::wstring mPolyImageFile;

    /// The underlying block image
    std::shared_ptr<wxImage> mPolyImage;

    /// The bitmap we can display for this block
    std::shared_ptr<wxBitmap> mPolyBitmap;

    /// Pointer to the physics object installed on this poly
    std::shared_ptr<Physics> mPhysics;



    /// Poly attribute to be loaded from the XML file
    b2Vec2 mPolySize;   ///< size of the poly
    b2Vec2 mPolyPosition; ///< position of the poly
    double mPolyWidth; ///< width of the poly
    double mPolyHeight; ///< height of the poly
    double mPolyAngle; ///< angle of the poly
    double mPolyFriction; ///< friction of the poly
    double mPolyRestitution; ///< restitution of the poly
    double mPolyDensity; ///< density of the poly
    int mPolyRepeatX;   ///< repetition of the poly
    std::wstring mType; ///< type of the poly

    /// Tells if the poly is a static body (true) or dynamic (false)
    bool mPolyStatic;
    
    /// Polys vertices
    std::vector<b2Vec2> mVertices;

public:
    /// Default constructor (disabled)
    Poly() = delete;

    /// Copy constructor
    Poly(const Poly &) = delete;

    /// Assignment operator
    void operator=(const Poly &) = delete;

    Poly(AngrySparty* sparty, wxXmlNode *node);

    void XmlLoad(wxXmlNode* node) override;

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void SetImage(const std::wstring& file) override;

    void InstallPhysics(std::shared_ptr<Physics> physics) override;


};

#endif //ANGRYSPARTY_POLY_H
