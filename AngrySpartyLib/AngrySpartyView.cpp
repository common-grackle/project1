/**
 * @file AngrySpartyView.cpp
 * @author ahmedna8, chenan12, parkeu24
 */

#include "pch.h"
#include "AngrySpartyView.h"
#include "Physics.h"
#include "ids.h"
#include <wx/dcbuffer.h>
#include <algorithm>
#include <wx/dc.h>
#include <wx/graphics.h>
#include "Consts.h"
#include "wx/wx.h"
#include "FoeDieCount.h"


using namespace std;

/// Frame Duration in Seconds
const double FrameDuration = 1.0/60.0;

/**
 * Handle timer events
 * @param event timer event
 */
void AngrySpartyView::OnTimer(wxTimerEvent& event)
{
    Refresh();
}

/**
 * Initialize the game view class.
 * @param parent The parent window for this class
 */
void AngrySpartyView::Initialize(wxFrame* parent)
{
    Create(parent, wxID_ANY,
            wxDefaultPosition, wxDefaultSize,
            wxFULL_REPAINT_ON_RESIZE);

    SetBackgroundStyle(wxBG_STYLE_PAINT);

    InitializeLoad();

    mAngrySparty.SetGameView(this);

    Bind(wxEVT_PAINT, &AngrySpartyView::OnPaint, this);
    Bind(wxEVT_TIMER, &AngrySpartyView::OnTimer, this);

    parent->Bind(wxEVT_COMMAND_MENU_SELECTED, &AngrySpartyView::LoadLevelZero, this, IDM_LEVELZERO);
    parent->Bind(wxEVT_COMMAND_MENU_SELECTED, &AngrySpartyView::LoadLevelOne, this, IDM_LEVELONE);
    parent->Bind(wxEVT_COMMAND_MENU_SELECTED, &AngrySpartyView::LoadLevelTwo, this, IDM_LEVELTWO);
    parent->Bind(wxEVT_COMMAND_MENU_SELECTED, &AngrySpartyView::LoadLevelThree, this, IDM_LEVELTHREE);

    Bind(wxEVT_LEFT_DOWN, &AngrySpartyView::OnLeftDown, this);
    Bind(wxEVT_LEFT_UP, &AngrySpartyView::OnLeftUp, this);
    Bind(wxEVT_MOTION, &AngrySpartyView::OnMouseMove, this);

    mTimer.SetOwner(this);
    mTimer.Start(int(FrameDuration * 1000));

    mStopWatch.Start();
}

/**
 * Paint event, draws the window.
 * @param event Paint event object
 */
void AngrySpartyView::OnPaint(wxPaintEvent& event)
{
    // Update until the game time matches
    // the current time.

    auto newTime = mStopWatch.Time() * 0.001;
    while(mTime < newTime)
    {
        mTime += FrameDuration;
        mAngrySparty.Update(FrameDuration);
    }


    wxAutoBufferedPaintDC dc(this);

    wxBrush background(*wxWHITE);
    dc.SetBackground(background);
    dc.Clear();

    auto size = GetClientSize();

    auto graphics = std::shared_ptr<wxGraphicsContext>(wxGraphicsContext::Create( dc ));
    graphics->SetInterpolationQuality(wxINTERPOLATION_BEST);
    mAngrySparty.OnDraw(graphics, size.GetWidth(), size.GetHeight());

}

/**
 * Load the game from a file
 * @param filename
 */
void AngrySpartyView::Load(wxString filename)
{
    mAngrySparty.Load(filename);
    Refresh();
}

/** 
* Load the level from the game
* @param level the level number to load
*/
void AngrySpartyView::LoadLevel(int level)
{
    mAngrySparty.LoadLevel(level);
    mAngrySparty.SetLevel(level);
    Refresh();
}

/**
* Initializes the load for the game
*/

void AngrySpartyView::InitializeLoad()
{
    for (int i = 0; i < 4; i++)
    {
        std::wstring levelFileName = L"levels/level" + std::to_wstring(i) + L".xml";
        this->Load(levelFileName);
    }

    this->LoadLevel(1);
}

/**
 * Handle the left mouse button down event
 * @param event
 */
void AngrySpartyView::OnLeftDown(wxMouseEvent &event)
{
    mAngrySparty.OnLeftDown(event);
}

/**
* Handle the left mouse button up event
* @param event
*/
void AngrySpartyView::OnLeftUp(wxMouseEvent &event)
{
    OnMouseMove(event);
}

/**
* Handle the mouse move event
* @param event
*/
void AngrySpartyView::OnMouseMove(wxMouseEvent &event)
{
    mAngrySparty.OnMouseMove(event);
    Refresh();
}





