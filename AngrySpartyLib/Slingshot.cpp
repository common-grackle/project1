/**
 * @file Slingshot.cpp
 * @author hayeskhl, parkeu24
 */

#include "pch.h"
#include "Slingshot.h"
#include "Consts.h"
#include <string>
#include <wx/graphics.h>
#include "Physics.h"

#include <string>
using namespace std;

/// Base filename for the slingshot image
const wstring WoodSlingshotBaseName = L"images/slingshot.png";

/// filename for the slingshot front image
const wstring WoodSlingshotFrontName = L"images/slingshot-front.png";

/// Size of the slingshot image in meters
const b2Vec2 WoodSlingshotSize = b2Vec2(0.5, 1.446);

/// Back band attachment point
const b2Vec2 WoodSlingshotBandAttachBack = b2Vec2(-0.15f, 1.12f);

/// Front band attachment point
const b2Vec2 WoodSlingshotBandAttachFront = b2Vec2(0.15f, 1.2f);

/// Maximum amount the slingshot can be pulled in meters
const double WoodSlingshotMaximumPull = 1;

/// Pull angles from -pi to this value are allowed
const double SlingshotMaximumNegativePullAngle = -1.7;

/// Pull angles from +pi to this value are allowed
const double SlingshotMinimumPositivePullAngle = 2.42;

/// Width of the slingshot band in centimeters
const int SlingshotBandWidth = 15;

/// The slingshot band colour
const wxColour SlingshotBandColor = wxColour(55, 18, 1);

/**
 * Constructor
 * @param sparty game this slingshot is a member of
 * @param node the node this slingshot is from
 */
Slingshot::Slingshot(AngrySparty* sparty, wxXmlNode *node) : Item(sparty)
{
    XmlLoad(node);
    mSlingshotImage = make_shared<wxImage>(WoodSlingshotBaseName, wxBITMAP_TYPE_ANY);
    mSlingshotBitmap = make_shared<wxBitmap>(*mSlingshotImage);

    mSlingshotFrontImage = make_shared<wxImage>(WoodSlingshotFrontName, wxBITMAP_TYPE_ANY);
    mSlingshotFrontBitmap = make_shared<wxBitmap>(*mSlingshotFrontImage);
}

/**
 * Draw this Slingshot
 * @param graphics Graphics context to draw on
 */
void Slingshot::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto wid = WoodSlingshotSize.x * Consts::MtoCM;
    auto hit = WoodSlingshotSize.y * Consts::MtoCM;
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mSlingshotBitmap;

    graphics->PushState();
    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap, x - (wid/2), - y - hit,wid, hit);
    graphics->PopState();


    //draw band here
    if(!mDrawSling) {
        graphics->PushState();
        wxPen slingshotPen(SlingshotBandColor, SlingshotBandWidth);
        graphics->SetPen(slingshotPen);

        double x1 = (WoodSlingshotBandAttachBack.x*Consts::MtoCM)+x;
        double y1 = (WoodSlingshotBandAttachBack.y*Consts::MtoCM)+y;
        double x2 = (WoodSlingshotBandAttachFront.x*Consts::MtoCM)+x;
        double y2 = (WoodSlingshotBandAttachFront.y*Consts::MtoCM)+y;
        graphics->StrokeLine(x1, y1, x2, y2);
        graphics->PopState();
    }

}


/**
 * Sets current bird to the first bird in the list
 * @param spartyBird First bird in list
 */
void Slingshot::SetBird(std::shared_ptr<SpartyBird> spartyBird)
{
    mCurrentBird = spartyBird;
}

/**
* Sets the background state of the slingshot
* @param graphics graphics object used to draw the background state
*/
void Slingshot::BackgroundState(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto wid = WoodSlingshotSize.x * Consts::MtoCM;
    auto hit = WoodSlingshotSize.y * Consts::MtoCM;
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mSlingshotBitmap;

    graphics->PushState();
    graphics->Scale(1, -1);
    graphics->DrawBitmap(*bitmap, x - (wid/2), - y - hit,wid, hit);

    graphics->PopState();
}

/**
 * Draws left band of slingshot
 * @param graphics
 */
void Slingshot::LeftBandState(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    //draw left band here


    wxPen slingshotPen(SlingshotBandColor, SlingshotBandWidth);
    graphics->SetPen(slingshotPen);

    double x1 = (WoodSlingshotBandAttachBack.x* Consts::MtoCM) + x;
    double y1 = (WoodSlingshotBandAttachBack.y* Consts::MtoCM) +  y;

    double x2 = mCurrentBird->GetX()*Consts::MtoCM;
    double y2 = mCurrentBird->GetY()*Consts::MtoCM;

    graphics->StrokeLine(x1, y1, x2, y2);

}

/**
 * Sets bird into Sling only when it was on the floor
 */
void Slingshot::BirdState()
{

    auto x = mX;
    auto y = mY;

    double x1 = (WoodSlingshotBandAttachBack.x) + x;
    double y1 = (WoodSlingshotBandAttachBack.y) +  y;
    double x2 = (WoodSlingshotBandAttachFront.x) + x;
    double y2 = (WoodSlingshotBandAttachFront.y) + y;



    double newX = (x1 + x2)/2;
    double newY = (y1 + y2)/2;



    mCurrentBird->SetLoadX(newX);
    mCurrentBird->SetLoadY(newY);

    mCurrentBird->SetBodyLocation(newX,newY);
}

/**
 * Draws the right band of the slingshot
 * @param graphics
 */
void Slingshot::RightBandState(std::shared_ptr<wxGraphicsContext> graphics)
{
    //draw band here
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    wxPen slingshotPen(SlingshotBandColor, SlingshotBandWidth);
    graphics->SetPen(slingshotPen);


    double x1 = mCurrentBird->GetX()*Consts::MtoCM;
    double y1 = mCurrentBird->GetY()*Consts::MtoCM;

    double x2 = (WoodSlingshotBandAttachFront.x* Consts::MtoCM) + x;
    double y2 = (WoodSlingshotBandAttachFront.y* Consts::MtoCM) + y;

    graphics->StrokeLine(x1, y1, x2, y2);
    graphics->StrokeLine(x1, y1, x1 - 1, y1);

}

/**
 * Final wooden part of slingshot drawn
 * @param graphics
 */
void Slingshot::FinalState(std::shared_ptr<wxGraphicsContext> graphics)
{
    auto wid = WoodSlingshotSize.x * Consts::MtoCM;
    auto hit = WoodSlingshotSize.y * Consts::MtoCM;
    auto x = mX * Consts::MtoCM;
    auto y = mY * Consts::MtoCM;

    std::shared_ptr<wxBitmap> bitmap = mSlingshotFrontBitmap;

    graphics->PushState();
    graphics->Scale(1, -1);
        graphics->DrawBitmap(*bitmap, x - (wid/2), - y - hit,wid, hit);

    graphics->PopState();
}

/**
 * Changes the location of the bird based on x and y coordinates
 * @param x
 * @param y
 */
void Slingshot::SetBirdLocation(double x, double y)
{
    b2Vec2 location = b2Vec2(x,y);


    double x1 = (WoodSlingshotBandAttachBack.x)+(mX);
    double y1 = (WoodSlingshotBandAttachBack.y)+(mY);
    double x2 = (WoodSlingshotBandAttachFront.x)+(mX);
    double y2 = (WoodSlingshotBandAttachFront.y)+(mY);

    b2Vec2 loadPoint = b2Vec2((x1+x2)/2,(y1+y2)/2);
    b2Vec2 toAngry = location - loadPoint;
    double theta = atan2(toAngry.y, toAngry.x);

    double bandLength = sqrt(pow(toAngry.x,2) + pow(toAngry.y,2));

    if (bandLength <= WoodSlingshotMaximumPull && ( (-M_PI <= theta && theta <=  SlingshotMaximumNegativePullAngle) || (SlingshotMinimumPositivePullAngle <= theta && theta <=  M_PI) ))
    {
        mCurrentBird->SetBodyLocation(x,y);
    }
    else
    {
        auto toAngryShortened = (WoodSlingshotMaximumPull/bandLength)*toAngry;
        auto newLocation = loadPoint + toAngryShortened;
        auto newToAngry = newLocation - loadPoint;
        double newTheta = atan2(newToAngry.y, newToAngry.x);
        double newBandLength = sqrt(pow(newToAngry.x,2) + pow(newToAngry.y,2));
        if (newBandLength <= WoodSlingshotMaximumPull && ( (-M_PI <= newTheta && newTheta <=  SlingshotMaximumNegativePullAngle) || (SlingshotMinimumPositivePullAngle <= newTheta && newTheta <=  M_PI) ))
        {
            mCurrentBird->SetBodyLocation(newLocation.x,newLocation.y);
        }

    }
}

/**
 * Sets bird physics to dynamic instead of static
 * @param physics Physics of game
 */

void Slingshot::SetDynamic(std::shared_ptr<Physics> physics)
{
    mCurrentBird->SetMoveable(TRUE);
    mCurrentBird->InstallPhysics(physics);
    mDrawSling = FALSE;
    mLoaded = FALSE;
}

/**
 * Slingshot Hit test to test if current bird is being clicked on
 * @param x
 * @param y
 * @return
 */

bool Slingshot::HitTest(double x, double y)
{
    return mCurrentBird->HitTest(x,y);
}

/**
 * brief Load the attributes for an item node.
 * @param node The Xml node we are loading the item from
 */
void Slingshot::XmlLoad(wxXmlNode* node)
{
    node->GetAttribute(L"x", L"0").ToDouble(&mX);
    node->GetAttribute(L"y", L"0").ToDouble(&mY);
}


/**
 *
 * Draws Slingshot bands and sets bird in slingshot when needed
 * @param graphics Graphics context for program
 *
 */
void Slingshot::Draw2(std::shared_ptr<wxGraphicsContext> graphics)
{

    if(mDrawSling)
    {
        LeftBandState(graphics);
    }
    b2Body* body = mCurrentBird->GetBody();
    if(!mLoaded && body->GetType() == b2_staticBody)
    {
        BirdState();
        mLoaded = TRUE;
        mDrawSling = TRUE;
    }
    if(mDrawSling)
    {
        RightBandState(graphics);
        FinalState(graphics);
    }
}