/**
 * @file Slingshot.h
 * @author hayeskhl,ahmedna8, parkeu24
 *
 *  This is the slingshot!
 */

#ifndef ANGRYSPARTY_SLINGSHOT_H
#define ANGRYSPARTY_SLINGSHOT_H

#include "Item.h"
#include "SpartyBird.h"

/**
* Class for the slingshot item in the game
*/
class Slingshot : public Item {
private:
    /// The bird currently in slingshot
    std::shared_ptr<SpartyBird> mCurrentBird;

    /// The underlying slingshot image
    std::shared_ptr<wxImage> mSlingshotImage;

    /// The front "prong" image of slingshot
    std::shared_ptr<wxImage> mSlingshotFrontImage;

    /// The bitmap we can display for this slingshot
    std::shared_ptr<wxBitmap> mSlingshotBitmap;

    /// The bitmap we can display for front "prong"
    std::shared_ptr<wxBitmap> mSlingshotFrontBitmap;

    /// x position of slingshot
    double mX = 0;

    /// y position of slingshot
    double mY = 0;

    /// The point to load the bird
    b2Vec2 mLoadPoint;

    /// tells if slingshot/band is static
    bool mStatic = true;

    /// Tells if slingshot is loaded
    bool mLoaded = FALSE;

    /// tells if slingshot band should be drawn
    bool mDrawSling = TRUE;
public:
    /// Default constructor (disabled)
    Slingshot() = delete;

    /// Copy constructor
    Slingshot(const Slingshot &) = delete;

    /// Assignment operator
    void operator=(const Slingshot &) = delete;

    Slingshot(AngrySparty* angrySparty, wxXmlNode *node);

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

    void XmlLoad(wxXmlNode* node) override;

    void SetBird(std::shared_ptr<SpartyBird> spartyBird) override;
    void BackgroundState(std::shared_ptr<wxGraphicsContext> graphics) override;
    void LeftBandState(std::shared_ptr<wxGraphicsContext> graphics) override;
    void BirdState() override;
    void RightBandState(std::shared_ptr<wxGraphicsContext> graphics) override;
    void FinalState(std::shared_ptr<wxGraphicsContext> graphics) override;

    bool HitTest(double x, double y) override;
    void SetBirdLocation(double x, double y) override;
    void SetDynamic(std::shared_ptr<Physics> physics) override;

    void Draw2(std::shared_ptr<wxGraphicsContext> graphics) override;


    /**
     * Set Loaded to false on level reset
     */
    void SetLoadedFalse(){mLoaded = FALSE;}
};

#endif //ANGRYSPARTY_SLINGSHOT_H
