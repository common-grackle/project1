/**
 * @file GruffSpartyBird.cpp
 * @author hayeskhl
 */

#include "pch.h"
#include "GruffSpartyBird.h"
#include "AngrySparty.h"
#include <wx/graphics.h>
#include "Consts.h"


using namespace std;

/// The image to use for this sparty
const std::wstring GruffSpartyImage = L"gruff-sparty.png";

/// The radius in meters
const double GruffSpartyRadius = 0.25;

/// The velocity factor for Gruff Sparty
const float GruffSpartyVelocityFactor = 12.0;

/**
 * Constructor
 * @param sparty game this bird is a member of
 * @param node the node this gruff bird is from
 */
GruffSpartyBird::GruffSpartyBird(AngrySparty* sparty, wxXmlNode *node) : SpartyBird(sparty, node)
{
    XmlLoad(node);
    SetImage(GruffSpartyImage);
}

/**
 * Sets the image file of the bird
 * @param file the image file of the bird
 */
void GruffSpartyBird::SetImage(const std::wstring& file)
{
    SpartyBird::SetImage(GruffSpartyImage);
}


/**
* brief Load the attributes for this bird node.
* @param node The Xml node we are loading the item from
*/
void GruffSpartyBird::XmlLoad(wxXmlNode* node)
{
    SpartyBird::XmlLoad(node);
}

/** Gets the birds velocity
* @return double the velocity of the gruff sparty bird
*/
double GruffSpartyBird::GetVelocity()
{
    mVelocity = GruffSpartyVelocityFactor;
    return mVelocity;
}
