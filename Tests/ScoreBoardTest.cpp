/**
 * @file ScoreBoardTest.cpp
 * @author Odon Mulambo
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <ScoreBoard.h>
#include <memory>

TEST(ScoreBoardTest, Getter) {
    ScoreBoard board;

    ASSERT_EQ(0, board.GetScore());

    board.IncreaseScore();
    ASSERT_EQ(100, board.GetScore());

    board.IncreaseScore();
    ASSERT_EQ(200, board.GetScore());
}