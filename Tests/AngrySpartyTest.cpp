/**
 * @file AngrySpartyTest.cpp
 * @author parkeu24
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <AngrySparty.h>
#include <Block.h>
#include <Foe.h>
#include <Slingshot.h>
#include <regex>
#include <string>
#include <fstream>
#include <wx/filename.h>
#include <cmath>

using namespace std;

class AngrySpartyTest : public ::testing::Test{
protected:

    /**
     * Test whether level 0 is loading properly to the AngrySparty from the xml file
     * There are total of 8 items in Level 0
     * @param angrySparty the game
     */
    void TestLevel0Load(AngrySparty &angrySparty)
    {
        angrySparty.LoadLevel(0);

        int numItems = angrySparty.GetNumItems();

        ASSERT_EQ(8, numItems);
    }

    /**
     * Test whether level 1 is loading properly to the AngrySparty from the xml file
     * There are total of 19 items in Level 1
     * @param angrySparty the game
     */
    void TestLevel1Load(AngrySparty &angrySparty)
    {
        angrySparty.LoadLevel(1);

        int numItems = angrySparty.GetNumItems();

        ASSERT_EQ(19, numItems);

        int numBirds = angrySparty.GetNumBirds();

        ASSERT_EQ(3, numBirds);
    }

    /**
    * Test whether level 2 is loading properly to the AngrySparty from the xml file
    * There are total of 24 items in Level 2
    * @param angrySparty the game
    */
    void TestLevel2Load(AngrySparty &angrySparty)
    {
        angrySparty.LoadLevel(2);

        int numItems = angrySparty.GetNumItems();

        ASSERT_EQ(24, numItems);

        int numBirds = angrySparty.GetNumBirds();

        ASSERT_EQ(6, numBirds);
    }

    /**
     * Test whether level 3 is loading properly to the AngrySparty from the xml file
     * There are total of 29 items in Level 3
     * @param angrySparty the game
     */
    void TestLevel3Load(AngrySparty &angrySparty)
    {
        angrySparty.LoadLevel(3);

        int numItems = angrySparty.GetNumItems();

        ASSERT_EQ(29, numItems);

        int numBirds = angrySparty.GetNumBirds();

        ASSERT_EQ(2, numBirds);
    }

    /**
     * Tests whether the level is reset
     * @param angrySparty the game
     */
     void TestLevelReset(AngrySparty &angrySparty)
    {
         angrySparty.Reset();

         int numItems = angrySparty.GetNumItems();

         ASSERT_EQ(0, numItems);

         int numBirds = angrySparty.GetNumBirds();

         ASSERT_EQ(0, numBirds);
    }

    /**
     * Tests whether reloaded level have bird as static instead of dynamic
     * @param angrySparty
     */
    void TestLevelReload(AngrySparty &angrySparty)
    {
         angrySparty.LoadLevel(0);
         bool moveable = angrySparty.GetMoveable();
         ASSERT_EQ(FALSE, moveable);
    }
};

TEST_F(AngrySpartyTest, Load) {
    // Create angry sparty
    AngrySparty angrySparty;

    // Loading all levels
    for (int i = 0; i < 4; i++)
    {
        std::wstring levelFileName = L"levels/level" + std::to_wstring(i) + L".xml";
        // Function Test #1) Loading all files at once
        angrySparty.Load(levelFileName);
    }

    // Function Test #2) Loading each individual level to the game
    TestLevel0Load(angrySparty);
    TestLevel1Load(angrySparty);
    TestLevel2Load(angrySparty);
    TestLevel3Load(angrySparty);
}

TEST_F(AngrySpartyTest, Reset)
{
    AngrySparty angrySparty;

    // Loading all levels
    for (int i = 0; i < 4; i++)
    {
        std::wstring levelFileName = L"levels/level" + std::to_wstring(i) + L".xml";
        angrySparty.Load(levelFileName);
    }

    angrySparty.LoadLevel(0);

    // Function Test #1) Resetting the level -> should clear all the mItems and mBirds associated with the level
    TestLevelReset(angrySparty);

    // Function Test #2) Testing to see if reloaded level birds have default values from the xml file
    TestLevelReload(angrySparty);


}
