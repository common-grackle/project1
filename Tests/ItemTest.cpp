/**
 * @file ItemTest.cpp
 * @author Andrew Chen
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <AngrySparty.h>
#include <Block.h>
#include <Poly.h>
#include <Item.h>
#include <Physics.h>


using namespace std;

class ItemTest : public ::testing::Test
{
protected:

    void TestItemPosition(AngrySparty* sparty,wxXmlNode *node)
    {
        shared_ptr<Item> item;

        item = make_shared<Block>(sparty, node);

        std::shared_ptr<Physics> physics = std::make_shared<Physics>(b2Vec2(1422,800));

        item->InstallPhysics(physics);
        b2Body* body = item->GetBody();
        auto position = body->GetPosition();

        ASSERT_NEAR(position.x, 3.0063 , 0.01);
        ASSERT_NEAR(position.y, 0.765003, 0.01);
    }

    void TestItemFixture(AngrySparty* sparty,wxXmlNode *node)
    {
        shared_ptr<Item> item;

        item = make_shared<Block>(sparty, node);

        std::shared_ptr<Physics> physics = std::make_shared<Physics>(b2Vec2(1422,800));

        item->InstallPhysics(physics);
        b2Body* body = item->GetBody();
        auto fixture = body->GetFixtureList();

        ASSERT_NEAR(fixture->GetFriction(), 0.3, 0.01);
        ASSERT_NEAR(fixture->GetRestitution(), 0.8, 0.01);
        ASSERT_NEAR(fixture->GetDensity(), 1, 0.01);

    }

};

TEST_F(ItemTest, InstallPhysics)
{
    AngrySparty angrySparty;

    wxXmlDocument xmlDoc;

    xmlDoc.Load(L"levels/level1.xml");

    auto root = xmlDoc.GetRoot();
    auto child = root->GetChildren();

    for ( ; child; child = child->GetNext())
    {
        auto name = child->GetName();
        if (name == L"items")
        {
            auto node = child->GetChildren();
            for(;node; node = node->GetNext())
            {
                auto type = node->GetName();
                if (type == L"block")
                {
                    if(node->GetAttribute(L"type") != L"static")
                    {
                        TestItemPosition(&angrySparty, node);
                        TestItemFixture(&angrySparty, node);
                        break;
                    }
                }
            }
        }
    }
}