/**
 * @file SpartyBirdTest.cpp
 * @author hayeskhl
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <AngrySparty.h>
#include <SpartyBird.h>
#include <wx/filename.h>

using namespace std;

TEST(SpartyBird, SetLocation){
    // Create the game and load levels
    AngrySparty angrySparty;

    // Loading all levels
    for (int i = 0; i < 4; i++)
    {
        std::wstring levelFileName = L"levels/level" + std::to_wstring(i) + L".xml";
        angrySparty.Load(levelFileName);
    }

    angrySparty.LoadLevel(0);

    // Function Test #1) Bird SetLocation in SpartyBird
    angrySparty.SetBirdLocation(0, 100.0, 200.0);

    // Function Test #2) Bird GetX() and GetY() in SpartyBird
    auto [birdX, birdY] = angrySparty.GetBirdLocation(0);

    ASSERT_EQ(100.0, birdX);
    ASSERT_EQ(200.0, birdY);

}